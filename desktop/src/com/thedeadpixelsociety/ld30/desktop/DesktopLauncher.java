package com.thedeadpixelsociety.ld30.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.thedeadpixelsociety.ld30.LD30Game;

public class DesktopLauncher
{
    public static void main(String[] arg)
    {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "LD30 - Connected Worlds: Nemesis";
        config.width = 1280;
        config.height = 720;
        new LwjglApplication(new LD30Game(), config);
    }
}
