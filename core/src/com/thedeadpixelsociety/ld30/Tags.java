package com.thedeadpixelsociety.ld30;

public final class Tags
{
    public static final int PLANETS = 2;
    public static final int PLAYER = 0;
    public static final int TETHERED_PLANET = 1;
    public static final int STAR = 3;
    public static final int FRAGMENTS = 4;
}
