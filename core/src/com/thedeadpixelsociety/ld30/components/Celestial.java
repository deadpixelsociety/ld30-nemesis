package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;

public class Celestial extends Component
{
    private float gravityFactor;

    public Celestial() { }

    public Celestial(final float gravityFactor)
    {
        this.gravityFactor = gravityFactor;
    }

    public float getGravityFactor()
    {
        return gravityFactor;
    }

    public void setGravityFactor(final float gravityFactor)
    {
        this.gravityFactor = gravityFactor;
    }
}
