package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;

public class Emits extends Component
{
    private ParticleType type;
    private float zOrder;

    public Emits() { }

    public Emits(final ParticleType type)
    {
        this.type = type;
    }

    public Emits(final ParticleType type, final float zOrder)
    {
        this.type = type;
        this.zOrder = zOrder;
    }

    public ParticleType getType()
    {
        return type;
    }

    public void setType(final ParticleType type)
    {
        this.type = type;
    }

    public float getzOrder()
    {
        return zOrder;
    }

    public void setzOrder(final float zOrder)
    {
        this.zOrder = zOrder;
    }

    public enum ParticleType
    {
        COLLISION,
        COMET_TAIL,
        EXPLOSION,
        EXPLOSION_SMALL
    }
}
