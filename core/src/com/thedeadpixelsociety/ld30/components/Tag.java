package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;
import com.badlogic.gdx.utils.IntArray;

public class Tag extends Component
{
    private final IntArray tags = new IntArray();

    public Tag() { }

    public Tag(final int... tags)
    {
        this.tags.addAll(tags);
    }

    public IntArray getTags()
    {
        return tags;
    }
}
