package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;

public class Health extends Component
{
    private float health;
    private float maxHealth;

    public Health() { }

    public Health(final float maxHealth)
    {
        this.maxHealth = this.health = maxHealth;
    }

    public float getHealth()
    {
        return health;
    }

    public void setHealth(final float health)
    {
        this.health = health;
    }

    public float getMaxHealth()
    {
        return maxHealth;
    }

    public void setMaxHealth(final float maxHealth)
    {
        this.maxHealth = maxHealth;
    }
}
