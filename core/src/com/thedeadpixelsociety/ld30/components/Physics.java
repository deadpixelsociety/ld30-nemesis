package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;
import com.badlogic.gdx.physics.box2d.Body;

public class Physics extends Component
{
    private Body body;

    public Physics() { }

    public Physics(final Body body)
    {
        this.body = body;
    }

    public Body getBody()
    {
        return body;
    }

    public void setBody(final Body body)
    {
        this.body = body;
    }
}
