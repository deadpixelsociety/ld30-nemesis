package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;
import com.thedeadpixelsociety.ld30.celestial.PlanetSize;
import com.thedeadpixelsociety.ld30.celestial.PlanetType;

public class Demographics extends Component
{
    private String dominateSpecies;
    private String name;
    private boolean populated;
    private long population;
    private PlanetSize size;
    private PlanetType type;

    public Demographics() { }

    public Demographics(final String name,
                        final PlanetType type,
                        final PlanetSize size,
                        final long population,
                        final String dominateSpecies)
    {
        this.name = name;
        this.type = type;
        this.size = size;
        this.population = population;
        this.dominateSpecies = dominateSpecies;
        populated = true;
    }

    public Demographics(final String name, final PlanetType type, final PlanetSize size)
    {
        this.name = name;
        this.type = type;
        this.size = size;

        dominateSpecies = "Uninhabited";
        populated = false;
    }

    public String getDominateSpecies()
    {
        return dominateSpecies;
    }

    public void setDominateSpecies(final String dominateSpecies)
    {
        this.dominateSpecies = dominateSpecies;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public long getPopulation()
    {
        return population;
    }

    public void setPopulation(final long population)
    {
        this.population = population;
    }

    public PlanetSize getSize()
    {
        return size;
    }

    public void setSize(final PlanetSize size)
    {
        this.size = size;
    }

    public PlanetType getType()
    {
        return type;
    }

    public void setType(final PlanetType type)
    {
        this.type = type;
    }

    public boolean isPopulated()
    {
        return populated;
    }

    public void setPopulated(final boolean populated)
    {
        this.populated = populated;
    }
}
