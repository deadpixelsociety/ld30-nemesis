package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Sprited extends Component
{
    private Sprite sprite;
    private Color tint = Color.WHITE;

    public Sprited() { }

    public Sprited(final Sprite sprite)
    {
        this.sprite = sprite;
    }

    public Sprited(final Sprite sprite, final Color tint)
    {
        this(sprite);

        this.tint = tint;
    }

    public Sprite getSprite()
    {
        return sprite;
    }

    public void setSprite(final Sprite sprite)
    {
        this.sprite = sprite;
    }

    public Color getTint()
    {
        return tint;
    }

    public void setTint(final Color tint)
    {
        this.tint = tint;
    }
}
