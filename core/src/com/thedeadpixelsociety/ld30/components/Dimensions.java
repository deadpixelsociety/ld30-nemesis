package com.thedeadpixelsociety.ld30.components;

import com.artemis.Component;

public class Dimensions extends Component
{
    private float height;
    private float radius;
    private float width;

    public Dimensions() { }

    public Dimensions(final float width, final float height)
    {
        this.width = width;
        this.height = height;
        radius = width <= height ? width : height;
    }

    public Dimensions(final float radius)
    {
        setRadius(radius);
    }

    public float getHeight()
    {
        return height;
    }

    public void setHeight(final float height)
    {
        this.height = height;
        radius = width <= height ? width : height;
    }

    public float getRadius()
    {
        return radius;
    }

    public void setRadius(final float radius)
    {
        this.radius = radius;
        this.width = this.height = radius;
    }

    public float getWidth()
    {
        return width;
    }

    public void setWidth(final float width)
    {
        this.width = width;
        radius = width <= height ? width : height;
    }
}
