package com.thedeadpixelsociety.ld30;

import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;
import java.util.Map;

public class GameServices implements Disposable
{
    private Map<Class<?>, Object> services = new HashMap<Class<?>, Object>();

    public void clear()
    {
        services.clear();
    }

    @Override public void dispose()
    {
        for(Map.Entry<Class<?>, Object> entry : services.entrySet())
        {
            Object service = entry.getValue();
            if(service instanceof Disposable)
            {
                Disposable disposable = (Disposable) service;
                disposable.dispose();
            }
        }

        clear();
    }

    public <T> T get(Class<T> serviceClass)
    {
        if(services.containsKey(serviceClass))
        {
            return (T) services.get(serviceClass);
        }

        return null;
    }

    public void put(Object service)
    {
        services.put(service.getClass(), service);
    }
}
