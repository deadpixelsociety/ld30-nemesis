/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.systems;

import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.*;

/** Camera control system. */
public class CameraSystem extends VoidEntitySystem
{
    private static final float TRACK_SPEED = .2f;
    private final Camera camera;
    private final Vector2 start = new Vector2();
    private final Vector2 target = new Vector2();
    private final Rectangle viewport = new Rectangle();
    private float panTime;
    private float panTimer;
    private boolean panning;
    private Vector2 tracking;
    private float zoomEnd;
    private float zoomStart;
    private float zoomTime;
    private float zoomTimer;
    private boolean zooming;

    /**
     * Constructs a {@link com.thedeadpixelsociety.ld30.systems.CameraSystem} object.
     *
     * @param camera
     *         The {@link com.badlogic.gdx.graphics.Camera} object.
     */
    public CameraSystem(Camera camera)
    {
        this.camera = camera;
    }

    /** Gets the camera. */
    public Camera getCamera()
    {
        return camera;
    }

    /**
     * Gets the camera viewport.
     *
     * @return The calculated viewport rectangle.
     */
    public Rectangle getViewport()
    {
        return getViewport(0f);
    }

    /**
     * Gets the camera viewport
     *
     * @param buffer
     *         The amount of buffer space around the viewport to add.
     *
     * @return The calculated viewport rectangle.
     */
    public Rectangle getViewport(float buffer)
    {
        float bufferWidth = camera.viewportWidth + buffer * 2;
        float bufferHeight = camera.viewportHeight + buffer * 2;
        float halfWidth = bufferWidth * .5f;
        float halfHeight = bufferHeight * .5f;
        float zoom = 1f;
        if(camera instanceof OrthographicCamera)
        {
            zoom = ((OrthographicCamera) camera).zoom;
        }
        viewport.set(camera.position.x - halfWidth * zoom,
                     camera.position.y - halfHeight * zoom,
                     bufferWidth * zoom,
                     bufferHeight * zoom);
        return viewport;
    }

    /**
     * Pans to the specified position over the specified amount of time.
     *
     * @param position
     *         The position.
     * @param time
     *         The amount of time (in seconds).
     */
    public void panTo(Vector2 position, float time)
    {
        if(time == 0f)
        {
            snapTo(position);
            return;
        }
        panning = true;
        panTimer = 0f;
        panTime = time;
        target.set(position);
        start.set(camera.position.x, camera.position.y);
    }

    /**
     * Zooms the camera to the specified level over the specified amount of time.
     *
     * @param zoom
     *         The zoom level.
     * @param time
     *         The amount of time (in seconds).
     */
    public void smoothZoom(float zoom, float time)
    {
        if(time == 0f)
        {
            zoomTo(zoom);
        }
        if(camera instanceof OrthographicCamera)
        {
            zooming = true;
            zoomTimer = 0f;
            zoomTime = time;
            zoomStart = ((OrthographicCamera) camera).zoom;
            zoomEnd = zoom;
        }
    }

    /**
     * Snaps the camera to the specified position.
     *
     * @param position
     *         The position.
     */
    public void snapTo(Vector2 position)
    {
        camera.position.set(position.x, position.y, 0f);
        camera.update();
    }

    /**
     * Tracks the specified position.
     *
     * @param vector
     *         The position.
     */
    public void track(Vector2 vector)
    {
        this.tracking = vector;
    }

    /**
     * Zooms the camera to the specified level.
     *
     * @param zoom
     *         The zoom level.
     */
    public void zoomTo(float zoom)
    {
        if(camera instanceof OrthographicCamera)
        {
            ((OrthographicCamera) camera).zoom = zoom;
            camera.update();
        }
    }

    @Override protected void processSystem()
    {
        final float dt = world.getDelta();

        track();
        zoom(dt);
        pan(dt);

        camera.update();
    }

    private void pan(final float dt)
    {
        if(!panning) return;
        panTimer += dt;
        if(panTimer >= panTime - .1f)
        {
            snapTo(target);
            panning = false;
            return;
        }
        float x = Interpolation.sineOut.apply(start.x, target.x, panTimer / panTime);
        float y = Interpolation.sineOut.apply(start.y, target.y, panTimer / panTime);
        camera.position.set(x, y, 0f);
        camera.update();
    }

    private void track()
    {
        if(tracking == null) return;
        panTo(tracking, TRACK_SPEED);
    }

    private void zoom(final float dt)
    {
        if(!zooming) return;
        zoomTimer += dt;
        if(zoomTimer >= zoomTime)
        {
            zoomTo(zoomEnd);
            zooming = false;
            return;
        }
        float z = Interpolation.linear.apply(zoomStart, zoomEnd, zoomTimer / zoomTime);
        zoomTo(z);
    }
}
