package com.thedeadpixelsociety.ld30.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.utils.*;
import com.thedeadpixelsociety.ld30.components.Tag;

public class TagSystem extends EntityProcessingSystem
{
    private final IntMap<Array<Entity>> tagMap = new IntMap<Array<Entity>>();
    @Mapper private ComponentMapper<Tag> tm;

    public TagSystem()
    {
        super(Aspect.getAspectForAll(Tag.class));
    }

    public Entity get(final int which, final int index)
    {
        final Array<Entity> all = getAll(which);
        if(all.size != 0 && index >= 0 && index < all.size)
        {
            return all.get(index);
        }

        return null;
    }

    public Array<Entity> getAll(final int which)
    {
        Array<Entity> entities = tagMap.get(which, null);
        if(entities == null)
        {
            entities = new Array<Entity>();
            tagMap.put(which, entities);
        }

        return entities;
    }

    public Entity getFirst(final int which)
    {
        final Array<Entity> all = getAll(which);
        if(all.size != 0)
        {
            return all.first();
        }

        return null;
    }

    public Entity getLast(final int which)
    {
        final Array<Entity> all = getAll(which);
        if(all.size != 0)
        {
            all.get(all.size - 1);
        }

        return null;
    }

    public void tag(final Entity entity, final int which)
    {
        final Tag tag = tm.getSafe(entity);

        if(tag != null &&
           !tag.getTags()
               .contains(which))
        {
            tag.getTags()
               .add(which);

            getAll(which).add(entity);
        }
    }

    public void untag(final Entity entity, final int which)
    {
        final Tag tag = tm.getSafe(entity);

        if(tag != null &&
           tag.getTags()
              .contains(which))
        {
            tag.getTags()
               .removeValue(which);

            getAll(which).removeValue(entity, true);
        }
    }

    @Override protected void inserted(final Entity e)
    {
        final Tag tag = tm.get(e);

        final IntArray tags = tag.getTags();

        for(int i = 0; i < tags.size; i++)
        {
            getAll(tags.get(i)).add(e);
        }
    }

    @Override protected void removed(final Entity e)
    {
        final Tag tag = tm.get(e);

        final IntArray tags = tag.getTags();

        for(int i = 0; i < tags.size; i++)
        {
            getAll(tags.get(i)).removeValue(e, true);
        }
    }

    @Override public boolean isPassive()
    {
        return true;
    }

    @Override protected void process(final Entity e)
    {

    }

    @Override public boolean checkProcessing()
    {
        return false;
    }
}
