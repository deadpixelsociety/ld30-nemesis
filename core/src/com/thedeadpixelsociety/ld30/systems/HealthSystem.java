package com.thedeadpixelsociety.ld30.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.thedeadpixelsociety.ld30.DeathSources;
import com.thedeadpixelsociety.ld30.components.Health;

public class HealthSystem extends EntityProcessingSystem
{
    private final Array<HealthEventListener> eventListeners = new Array<HealthEventListener>();
    @Mapper private ComponentMapper<Health> hm;

    public HealthSystem()
    {
        super(Aspect.getAspectForAll(Health.class));
    }

    public void addListener(final HealthEventListener listener)
    {
        eventListeners.add(listener);
    }

    public void damage(final Entity entity, final float damage)
    {
        final Health health = hm.getSafe(entity);
        if(health != null)
        {
            addHealth(health, -damage);
            onDamaged(entity, health, damage);
        }
    }

    public Color getHealthColor(final Entity entity)
    {
        final Health health = hm.getSafe(entity);
        if(health != null)
        {
            final float healthPercent = health.getHealth() / health.getMaxHealth();

            // Green to yellow
            if(healthPercent == 1f)
            {
                return Color.GREEN;
            }
            else if(healthPercent < 1f && healthPercent >= .5f)
            {
                return new Color(1f - healthPercent, 1f, 0f, 1f);
            }
            else if(healthPercent < .5f && healthPercent > 0f)
            {
                return new Color(1f, healthPercent, 0f, 1f);
            }
            else
            {
                return Color.RED;
            }
        }

        return Color.WHITE;
    }

    public void heal(final Entity entity, final float heal)
    {
        final Health health = hm.getSafe(entity);
        if(health != null)
        {
            addHealth(health, heal);
            onHealed(entity, health, heal);
        }
    }

    public void kill(final Entity entity, final int source)
    {
        final Health health = hm.getSafe(entity);
        if(health != null)
        {
            setHealth(health, 0f);
            onKilled(entity, health, source);
        }
    }

    public void restore(final Entity entity)
    {
        final Health health = hm.getSafe(entity);
        if(health != null)
        {
            setHealth(health, health.getMaxHealth());
            onRestored(entity, health);
        }
    }

    @Override protected void process(final Entity e)
    {
        Health health = hm.get(e);

        if(health.getHealth() <= 0)
        {
            kill(e, DeathSources.DAMAGE);
        }
    }

    private void addHealth(final Health health, final float amount)
    {
        health.setHealth(Math.max(0f, Math.min(health.getHealth() + amount, health.getMaxHealth())));
    }

    private void onDamaged(final Entity entity, final Health health, final float damage)
    {
        for(HealthEventListener listener : eventListeners)
        {
            listener.onDamaged(entity, health, damage);
        }
    }

    private void onHealed(final Entity entity, final Health health, final float heal)
    {
        for(HealthEventListener listener : eventListeners)
        {
            listener.onHealed(entity, health, heal);
        }
    }

    private void onKilled(final Entity entity, final Health health, final int source)
    {
        for(HealthEventListener listener : eventListeners)
        {
            listener.onKilled(entity, health, source);
        }
    }

    private void onRestored(final Entity entity, final Health health)
    {
        for(HealthEventListener listener : eventListeners)
        {
            listener.onRestored(entity, health);
        }
    }

    private void setHealth(final Health health, final float amount)
    {
        health.setHealth(Math.max(0f, Math.min(amount, health.getMaxHealth())));
    }

    public static interface HealthEventListener
    {
        boolean onDamaged(final Entity entity, final Health health, final float amount);

        boolean onHealed(final Entity entity, final Health health, final float amount);

        boolean onKilled(final Entity entity, final Health health, final int source);

        boolean onRestored(final Entity entity, final Health health);
    }
}
