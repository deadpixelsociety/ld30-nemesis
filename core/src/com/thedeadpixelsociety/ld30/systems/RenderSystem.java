package com.thedeadpixelsociety.ld30.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.thedeadpixelsociety.ld30.components.*;

public class RenderSystem extends EntityProcessingSystem
{
    private final SpriteBatch spriteBatch;
    private Camera camera;
    @Mapper private ComponentMapper<Dimensions> dm;
    @Mapper private ComponentMapper<Emits> em;
    private ParticleSystem particleSystem;
    @Mapper private ComponentMapper<Physics> pm;
    @Mapper private ComponentMapper<Sprited> sm;

    public RenderSystem(final SpriteBatch spriteBatch)
    {
        super(Aspect.getAspectForAll(Sprited.class, Physics.class, Dimensions.class));
        this.spriteBatch = spriteBatch;
    }

    @Override protected void begin()
    {
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
    }

    @Override protected void end()
    {
        for(Emits.ParticleType type : Emits.ParticleType.values())
        {
            final Array<ParticleEffectPool.PooledEffect> effects = particleSystem.getUnattachedEffectsOfType(type);
            if(effects.size > 0)
            {
                for(ParticleEffectPool.PooledEffect effect : effects)
                {
                    effect.draw(spriteBatch);
                }
            }
        }

        spriteBatch.end();
    }

    @Override protected void initialize()
    {
        camera = world.getSystem(CameraSystem.class)
                      .getCamera();
        particleSystem = world.getSystem(ParticleSystem.class);
    }

    @Override protected void process(final Entity entity)
    {
        final Sprited sprited = sm.get(entity);
        final Physics physics = pm.get(entity);
        final Dimensions dimensions = dm.get(entity);
        final Emits emits = em.getSafe(entity);

        ParticleEffect particleEffect = null;
        if(emits != null)
        {
            particleEffect = particleSystem.getEffectOfType(entity, emits.getType());
        }

        final Sprite sprite = sprited.getSprite();
        if(sprite != null)
        {
            final Vector2 position = physics.getBody()
                                            .getPosition();

            sprite.setColor(sprited.getTint());
            sprite.setRotation(MathUtils.radiansToDegrees *
                               physics.getBody()
                                      .getAngle());
            sprite.setPosition(position.x - sprite.getOriginX(), position.y - sprite.getOriginY());

            if(emits != null && particleEffect != null && emits.getzOrder() < 0f)
            {
                particleEffect.draw(spriteBatch);
            }

            sprite.draw(spriteBatch);

            if(emits != null && particleEffect != null && emits.getzOrder() >= 0f)
            {
                particleEffect.draw(spriteBatch);
            }
        }
    }
}
