package com.thedeadpixelsociety.ld30.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.thedeadpixelsociety.ld30.components.Emits;
import com.thedeadpixelsociety.ld30.components.Physics;
import com.thedeadpixelsociety.ld30.graphics.Units;

public class ParticleSystem extends EntityProcessingSystem
{
    @Mapper private ComponentMapper<Emits> em;
    private ObjectMap<Entity, ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect>> entityEffects = new ObjectMap<Entity, ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect>>();
    private ObjectMap<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>> finishedParticles = new ObjectMap<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>>();
    @Mapper private ComponentMapper<Physics> pm;
    private ObjectMap<Emits.ParticleType, ParticleEffectPool> pools = new ObjectMap<Emits.ParticleType,
                                                                                           ParticleEffectPool>();
    private ObjectMap<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>> unattachedEffects = new ObjectMap<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>>();

    public ParticleSystem()
    {
        super(Aspect.getAspectForAll(Emits.class, Physics.class));
    }

    public void addEffect(final Entity entity,
                          final Emits.ParticleType type,
                          final ParticleEffectPool.PooledEffect effect)
    {
        final ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect> effects = getEffectsFor(entity);

        if(!effects.containsKey(type))
        {
            effects.put(type, effect);
        }
    }

    public Array<ParticleEffectPool.PooledEffect> getUnattachedEffectsOfType(final Emits.ParticleType type)
    {
        Array<ParticleEffectPool.PooledEffect> effects = unattachedEffects.get(type);
        if(effects == null)
        {
            effects = new Array<ParticleEffectPool.PooledEffect>();
            unattachedEffects.put(type, effects);
        }

        return effects;
    }

    public void addUnattachedEffect(final Emits.ParticleType type, final ParticleEffectPool.PooledEffect effect)
    {
        getUnattachedEffectsOfType(type).add(effect);
    }

    public ParticleEffectPool.PooledEffect createEffect(final Emits.ParticleType type)
    {
        return pools.get(type)
                    .obtain();
    }

    public ParticleEffectPool.PooledEffect getEffectOfType(final Entity entity, final Emits.ParticleType type)
    {
        final ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect> effects = getEffectsFor(entity);

        for(ObjectMap.Entry<Emits.ParticleType, ParticleEffectPool.PooledEffect> entry : effects)
        {
            if(entry.key == type)
            {
                return entry.value;
            }
        }

        return null;
    }

    public ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect> getEffectsFor(final Entity entity)
    {
        ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect> effects = entityEffects.get(entity, null);
        if(effects == null)
        {
            effects = new ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect>();
            entityEffects.put(entity, effects);
        }

        return effects;
    }

    public void removeEffect(final Entity entity, final Emits.ParticleType type)
    {
        final ObjectMap<Emits.ParticleType, ParticleEffectPool.PooledEffect> effects = getEffectsFor(entity);

        effects.remove(type);
    }

    @Override protected void end()
    {
        for(ObjectMap.Entry<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>> entry : finishedParticles)
        {
            final Array<ParticleEffectPool.PooledEffect> effects = entry.value;
            if(effects != null)
            {
                effects.clear();
            }
        }

        for(ObjectMap.Entry<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>> entry : unattachedEffects)
        {
            final Array<ParticleEffectPool.PooledEffect> effects = entry.value;
            if(effects != null)
            {
                for(ParticleEffectPool.PooledEffect effect : effects)
                {
                    effect.update(world.getDelta());
                    if(effect.isComplete())
                    {
                        finishedParticles.get(entry.key)
                                         .add(effect);
                    }
                }
            }
        }

        for(ObjectMap.Entry<Emits.ParticleType, Array<ParticleEffectPool.PooledEffect>> entry : finishedParticles)
        {
            final Array<ParticleEffectPool.PooledEffect> effects = entry.value;
            if(effects != null)
            {
                for(ParticleEffectPool.PooledEffect effect : effects)
                {
                    unattachedEffects.get(entry.key)
                                     .removeValue(effect, true);
                    effect.free();
                }
            }
        }
    }

    @Override protected void initialize()
    {
        for(Emits.ParticleType type : Emits.ParticleType.values())
        {
            finishedParticles.put(type, new Array<ParticleEffectPool.PooledEffect>());
        }

        final ParticleEffect explosion = new ParticleEffect();
        explosion.load(Gdx.files.internal("particles/explosion.p"), Gdx.files.internal("particles/images"));
        for(ParticleEmitter emitter : explosion.getEmitters())
        {
            final Sprite sprite = emitter.getSprite();
            sprite.setSize(Units.instance.getUnits(sprite.getWidth()), Units.instance.getUnits(sprite.getHeight()));
            sprite.setOriginCenter();
        }
        pools.put(Emits.ParticleType.EXPLOSION, new ParticleEffectPool(explosion, 10, 20));

        final ParticleEffect smExplosion = new ParticleEffect();
        smExplosion.load(Gdx.files.internal("particles/small-explosion.p"), Gdx.files.internal("particles/images"));
        for(ParticleEmitter emitter : smExplosion.getEmitters())
        {
            final Sprite sprite = emitter.getSprite();
            sprite.setSize(Units.instance.getUnits(sprite.getWidth()), Units.instance.getUnits(sprite.getHeight()));
            sprite.setOriginCenter();
        }
        pools.put(Emits.ParticleType.EXPLOSION_SMALL, new ParticleEffectPool(smExplosion, 10, 20));

        /*
        final ParticleEffect cometTail = new ParticleEffect();
        cometTail.load(Gdx.files.internal("particles/cometTail.p"), Gdx.files.internal("particles/images"));
        pools.put(Emits.ParticleType.COMET_TAIL, new ParticleEffectPool(cometTail, 3, 5));

        final ParticleEffect collision = new ParticleEffect();
        collision.load(Gdx.files.internal("particles/collision.p"), Gdx.files.internal("particles/images"));
        pools.put(Emits.ParticleType.COLLISION, new ParticleEffectPool(collision, 3, 5));
        */
    }

    @Override protected void inserted(final Entity e)
    {
        final Emits emits = em.get(e);

        switch(emits.getType())
        {
            case COLLISION:
                break;
            case COMET_TAIL:
                break;
            case EXPLOSION:
                break;
        }
    }

    @Override protected void process(final Entity entity)
    {
        final Emits emits = em.get(entity);
        final Physics physics = pm.get(entity);

        final ParticleEffectPool.PooledEffect effect = getEffectOfType(entity, emits.getType());

        if(effect != null)
        {
            switch(emits.getType())
            {
                case COMET_TAIL:
                    updateCometTail(entity, effect, physics);
                    break;
            }

            effect.update(world.getDelta());
        }
    }

    private void updateCometTail(final Entity entity,
                                 final ParticleEffectPool.PooledEffect effect,
                                 final Physics physics)
    {
        if(effect != null)
        {
            for(ParticleEmitter emitter : effect.getEmitters())
            {
                //emitter.getAngle().set
            }
        }
    }
}
