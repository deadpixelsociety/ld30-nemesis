package com.thedeadpixelsociety.ld30.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.*;
import com.badlogic.gdx.utils.*;
import com.thedeadpixelsociety.ld30.DeathSources;
import com.thedeadpixelsociety.ld30.Tags;
import com.thedeadpixelsociety.ld30.celestial.SolarSystem;
import com.thedeadpixelsociety.ld30.components.*;

public class PhysicsSystem extends EntityProcessingSystem implements Disposable
{
    private static final float GRAVITY = 20f;
    final Array<Body> gravityInfluenced = new Array<Body>();
    private final Array<Body> bodies = new Array<Body>();
    private final World box2d;
    private final Box2DDebugRenderer renderer;
    private final SolarSystem solarSystem;
    @Mapper private ComponentMapper<Celestial> cm;
    private ContactListener contactListener = new ContactListener()
    {
        private ObjectMap<Body, Body> lastCollided = new ObjectMap<Body, Body>();

        @Override public void beginContact(final Contact contact)
        {
            final Body bodyA = contact.getFixtureA()
                                      .getBody();
            final Body bodyB = contact.getFixtureB()
                                      .getBody();

            if((lastCollided.containsKey(bodyA) && lastCollided.get(bodyA) == bodyB) ||
               (lastCollided.containsKey(bodyB) && lastCollided.get(bodyB) == bodyA))
            {
                return;
            }
            else
            {
                lastCollided.put(bodyA, bodyB);
                lastCollided.put(bodyB, bodyA);
            }

            Entity entityA = null;
            Entity entityB = null;

            if(bodyA.getUserData() instanceof Entity)
            {
                entityA = (Entity) bodyA.getUserData();
            }

            if(bodyB.getUserData() instanceof Entity)
            {
                entityB = (Entity) bodyB.getUserData();
            }

            if(entityA != null && entityB != null)
            {
                final Tag tagA = tm.getSafe(entityA);
                final Tag tagB = tm.getSafe(entityB);

                if(tagA != null && tagB != null)
                {
                    final boolean aIsPlanet = tagA.getTags()
                                                  .contains(Tags.PLANETS);
                    final boolean aIsStar = tagA.getTags()
                                                .contains(Tags.STAR);
                    final boolean aIsPlayer = tagA.getTags()
                                                  .contains(Tags.PLAYER);
                    final boolean bIsPlanet = tagB.getTags()
                                                  .contains(Tags.PLANETS);
                    final boolean bIsPlayer = tagB.getTags()
                                                  .contains(Tags.PLAYER);
                    final boolean bIsFragment = tagB.getTags()
                                                    .contains(Tags.FRAGMENTS);

                    if(aIsStar && bIsPlanet)
                    {
                        healthSystem.kill(entityB, DeathSources.STAR);
                    }
                    else if((aIsPlanet && (bIsPlanet || bIsPlayer)) || (aIsPlayer && bIsPlanet))
                    {
                        final float damageA = calculateDamage(entityB, entityA);
                        final float damageB = calculateDamage(entityA, entityB);

                        healthSystem.damage(entityA, damageA);
                        healthSystem.damage(entityB, damageB);

                        breakStarJoint(bodyA);
                        breakStarJoint(bodyB);
                    }
                    else if((aIsPlanet || aIsStar || aIsPlayer) && bIsFragment)
                    {
                        healthSystem.kill(entityB, DeathSources.COLLISION);
                    }
                    else if(aIsStar && bIsPlayer)
                    {
                        healthSystem.kill(entityB, DeathSources.STAR);
                    }

                    if(aIsPlanet && bIsPlanet)
                    {
                        Body tetheredBody = null;
                        Body otherBody = null;
                        if(tagA.getTags()
                               .contains(Tags.TETHERED_PLANET))
                        {
                            tetheredBody = bodyA;
                            otherBody = bodyB;
                        }
                        else if(tagB.getTags()
                                    .contains(Tags.TETHERED_PLANET))
                        {
                            tetheredBody = bodyB;
                            otherBody = bodyA;
                        }

                        if(tetheredBody != null)
                        {
                            final Vector2 force = otherBody.getPosition()
                                                           .sub(tetheredBody.getPosition())
                                                           .nor()
                                                           .scl(tetheredBody.getLinearVelocity()
                                                                            .len());
                            otherBody.setLinearVelocity(force.scl(.6f));
                        }
                    }
                }
            }
        }

        @Override public void endContact(final Contact contact)
        {
            lastCollided.remove(contact.getFixtureA()
                                       .getBody());
            lastCollided.remove(contact.getFixtureB()
                                       .getBody());
        }

        @Override public void preSolve(final Contact contact, final Manifold oldManifold)
        {

        }

        @Override public void postSolve(final Contact contact, final ContactImpulse impulse)
        {

        }

        private void breakStarJoint(final Body body)
        {
            for(JointEdge jointEdge : body.getJointList())
            {
                if(jointEdge.joint instanceof DistanceJoint)
                {
                    if(jointEdge.other != null && jointEdge.other.getUserData() instanceof Entity)
                    {
                        final Entity entity = (Entity) jointEdge.other.getUserData();
                        final Tag tag = tm.getSafe(entity);
                        if(tag != null &&
                           tag.getTags()
                              .contains(Tags.STAR))
                        {
                            if(!jointsToDestroy.contains(jointEdge.joint, true))
                            {
                                jointsToDestroy.add(jointEdge.joint);
                            }
                        }
                    }
                }
            }

            body.setLinearDamping(.3f);
            body.setAngularDamping(.3f);
        }

        private float calculateDamage(final Entity a, final Entity b)
        {
            final Physics physicsA = pm.getSafe(a);
            final Physics physicsB = pm.getSafe(b);
            final Demographics demographicsA = dm.getSafe(a);
            final Demographics demographicsB = dm.getSafe(b);

            if(physicsA != null && physicsB != null && demographicsA != null && demographicsB != null)
            {
                final int rankA = demographicsA.getSize()
                                               .getRank();
                final int rankB = demographicsB.getSize()
                                               .getRank();

                float multipler = 0f;
                // Damage-dealing planet is larger than the receiving planet, so it gets
                // a damage multiplier.
                if(rankA > rankB)
                {
                    multipler = rankA - rankB;
                }

                final float speedA = physicsA.getBody()
                                             .getLinearVelocity()
                                             .len();
                final float speedB = physicsB.getBody()
                                             .getLinearVelocity()
                                             .len();
                final float avgSpeed = (speedA + speedB) * .5f;

                float damage = ((avgSpeed * .5f) / 75) * 50f;

                if(multipler != 0f)
                {
                    damage += (damage * multipler);
                }

                return damage;
            }

            return 0;
        }
    };
    @Mapper private ComponentMapper<Demographics> dm;
    private HealthSystem healthSystem;
    private Array<Joint> jointsToDestroy = new Array<Joint>();
    @Mapper private ComponentMapper<Physics> pm;
    @Mapper private ComponentMapper<Tag> tm;

    public PhysicsSystem(final SolarSystem solarSystem)
    {
        super(Aspect.getAspectForAll(Physics.class));

        this.solarSystem = solarSystem;

        box2d = new World(Vector2.Zero, false);
        renderer = new Box2DDebugRenderer();
        renderer.setDrawJoints(false);

        box2d.setContactListener(contactListener);
    }

    public Body createBody(final BodyDef bodyDef)
    {
        return box2d.createBody(bodyDef);
    }

    public Body createCircleBody(final float radius, BodyDef.BodyType bodyType, final float x, final float y)
    {
        final CircleShape circleShape = new CircleShape();
        circleShape.setRadius(radius);

        final BodyDef bodyDef = new BodyDef();
        bodyDef.type = bodyType;
        bodyDef.position.set(x, y);

        final Body body = createBody(bodyDef);
        body.createFixture(circleShape, 1f);

        circleShape.dispose();

        return body;
    }

    public Joint createDistanceJoint(final Body a, final Body b, final float length)
    {
        final DistanceJointDef jointDef = new DistanceJointDef();
        jointDef.bodyA = a;
        jointDef.bodyB = b;
        jointDef.length = length;

        return createJoint(jointDef);
    }

    public Joint createJoint(final JointDef jointDef)
    {
        return box2d.createJoint(jointDef);
    }

    public Joint createRopeJoint(final Body a, final Body b, final float maxLength)
    {
        final RopeJointDef jointDef = new RopeJointDef();
        jointDef.bodyA = a;
        jointDef.bodyB = b;
        jointDef.maxLength = maxLength;

        return createJoint(jointDef);
    }

    public void destroyBody(final Body body)
    {
        box2d.destroyBody(body);
    }

    public void destroyJoint(final Joint joint)
    {
        box2d.destroyJoint(joint);
    }

    @Override public void dispose()
    {
        box2d.dispose();
        renderer.dispose();
    }

    public World getBox2d()
    {
        return box2d;
    }

    public Box2DDebugRenderer getRenderer()
    {
        return renderer;
    }

    public void query(final QueryCallback queryCallback,
                      final float x,
                      final float y,
                      final float width,
                      final float height)
    {
        box2d.QueryAABB(queryCallback, x, y, x + width, y + height);
    }

    public void renderDebug(Matrix4 projection)
    {
        renderer.render(box2d, projection);
    }

    @Override protected void end()
    {
        jointsToDestroy.clear();

        box2d.step(world.getDelta(), 5, 3);

        for(Joint joint : jointsToDestroy)
        {
            destroyJoint(joint);
        }

        wrapBodies();
    }

    @Override protected void initialize()
    {
        healthSystem = world.getSystem(HealthSystem.class);
    }

    @Override protected void removed(final Entity e)
    {
        final Physics physics = pm.get(e);

        box2d.destroyBody(physics.getBody());
    }

    @Override protected void process(final Entity e)
    {
        final Physics physics = pm.get(e);
        final Celestial celestial = cm.getSafe(e);
        final Demographics demographics = dm.getSafe(e);
        final Vector2 position = physics.getBody()
                                        .getPosition();

        gravityInfluenced.clear();

        if(celestial != null && demographics != null)
        {
            float radius = demographics.getSize()
                                       .getRadius();
            radius += (radius * .5f) *
                      demographics.getSize()
                                  .getRank();

            final float gravity = GRAVITY * celestial.getGravityFactor();

            box2d.QueryAABB(new QueryCallback()
            {
                @Override public boolean reportFixture(final Fixture fixture)
                {
                    final Body body = fixture.getBody();
                    if(body.getUserData() instanceof Entity)
                    {
                        final Entity entity = (Entity) body.getUserData();
                        final Tag tag = entity.getComponent(Tag.class);
                        if(tag != null &&
                           tag.getTags()
                              .contains(Tags.FRAGMENTS))
                        {
                            gravityInfluenced.add(body);
                        }
                    }

                    return true;
                }
            }, position.x - radius, position.y - radius, radius * 2f, radius * 2f);

            if(gravityInfluenced.size > 0)
            {
                for(Body body : gravityInfluenced)
                {
                    final Vector2 force = position.sub(body.getPosition())
                                                  .nor()
                                                  .scl(gravity);
                    body.applyLinearImpulse(force, body.getWorldCenter(), true);
                }
            }
        }
    }

    private void wrapBodies()
    {
        box2d.getBodies(bodies);

        final Rectangle bounds = solarSystem.getBounds();

        final Vector2 newPosition = new Vector2();

        for(Body body : bodies)
        {
            final Vector2 position = body.getPosition();
            newPosition.set(position);

            if(position.x < bounds.x)
            {
                newPosition.x = bounds.x + bounds.width;
            }

            if(position.x > bounds.x + bounds.width)
            {
                newPosition.x = bounds.x;
            }

            if(position.y < bounds.y)
            {
                newPosition.y = bounds.y + bounds.height;
            }

            if(position.y > bounds.y + bounds.height)
            {
                newPosition.y = bounds.y;
            }

            if(!position.epsilonEquals(newPosition, .001f))
            {
                boolean isTethered = false;
                if(body.getUserData() instanceof Entity)
                {
                    final Entity entity = (Entity) body.getUserData();
                    final Tag tag = tm.getSafe(entity);
                    if(tag != null &&
                       tag.getTags()
                          .contains(Tags.PLAYER))
                    {
                        final Entity tetheredEntity = world.getSystem(TagSystem.class)
                                                           .getFirst(Tags.TETHERED_PLANET);
                        if(tetheredEntity != null)
                        {
                            final Physics physics = pm.getSafe(tetheredEntity);
                            if(physics != null)
                            {
                                physics.getBody()
                                       .setTransform(newPosition,
                                                     physics.getBody()
                                                            .getAngle());
                            }
                        }
                    }
                    else if(tag != null &&
                            tag.getTags()
                               .contains(Tags.TETHERED_PLANET))
                    {
                        isTethered = true;
                    }
                }

                if(!isTethered)
                {
                    body.setTransform(newPosition, body.getAngle());
                }
            }
        }
    }
}
