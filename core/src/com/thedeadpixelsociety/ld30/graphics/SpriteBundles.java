/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.graphics;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

public final class SpriteBundles
{
    public static final SpriteBundles instance = new SpriteBundles();
    private static final float FRAME_DURATION = 1f / 10f;
    private final ObjectMap<String, Animation> animationMap = new ObjectMap<String, Animation>();
    private final ObjectMap<String, Sprite> spriteMap = new ObjectMap<String, Sprite>();

    private SpriteBundles() { }

    public void create(final TextureAtlas textureAtlas)
    {
        create(textureAtlas, true);
    }

    public void create(final TextureAtlas textureAtlas, final boolean autoSize)
    {
        for(TextureAtlas.AtlasRegion atlasRegion : textureAtlas.getRegions())
        {
            if(atlasRegion.index == -1)
            {
                createSprite(atlasRegion, textureAtlas, autoSize);
            }
            else
            {
                createAnimation(atlasRegion, textureAtlas, autoSize);
            }
        }
    }

    public Animation getAnimation(final String name)
    {
        return animationMap.get(name);
    }

    public Sprite getSprite(final String name)
    {
        return spriteMap.get(name);
    }

    private void createAnimation(final TextureAtlas.AtlasRegion atlasRegion,
                                 final TextureAtlas textureAtlas,
                                 final boolean autoSize)
    {
        Array<Sprite> sprites = textureAtlas.createSprites(atlasRegion.name);
        if(autoSize)
        {
            for(Sprite sprite : sprites)
            {
                sprite.setSize(Units.instance.getUnits(sprite.getWidth()), Units.instance.getUnits(sprite.getHeight()));
                sprite.setOrigin(sprite.getWidth() * .5f, sprite.getHeight() * .5f);
            }
        }
        Animation animation = new Animation(FRAME_DURATION, sprites);
        animationMap.put(atlasRegion.name, animation);
    }

    private void createSprite(final TextureAtlas.AtlasRegion atlasRegion,
                              final TextureAtlas textureAtlas,
                              final boolean autoSize)
    {
        Sprite sprite = textureAtlas.createSprite(atlasRegion.name);
        if(autoSize)
        {
            sprite.setSize(Units.instance.getUnits(sprite.getWidth()), Units.instance.getUnits(sprite.getHeight()));
            sprite.setOrigin(sprite.getWidth() * .5f, sprite.getHeight() * .5f);
        }
        spriteMap.put(atlasRegion.name, sprite);
    }
}
