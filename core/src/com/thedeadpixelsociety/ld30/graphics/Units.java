/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.graphics;

/** Singleton class used to work with world units. */
public final class Units {
    /** The {@link com.thedeadpixelsociety.ld30.graphics.Units} singleton instance. */
    public static final Units instance = new Units();
    private int pixelsPerUnit = 1;

    /** Constructs a {@link com.thedeadpixelsociety.ld30.graphics.Units} object. */
    private Units() { }

    /**
     * Gets the number of pixels per unit.
     *
     * @return The number of pixels per unit.
     */
    public int getPixelsPerUnit() {
        return pixelsPerUnit;
    }

    /**
     * Sets the number of pixels per unit.
     *
     * @param pixelsPerUnit
     *         The number of pixels per unit.
     */
    public void setPixelsPerUnit(int pixelsPerUnit) {
        this.pixelsPerUnit = pixelsPerUnit;
    }

    /**
     * Gets the number of units in the specified pixels.
     *
     * @param pixels
     *         The number of pixels.
     * @return The number of units.
     */
    public float getUnits(float pixels) {
        return (float)(pixels / pixelsPerUnit);
    }
}
