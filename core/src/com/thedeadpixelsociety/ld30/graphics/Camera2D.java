/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;

/** An expanded 2D camera. */
public class Camera2D extends OrthographicCamera
{
    public static final float MIN_ZOOM = 2.5f;

    /**
     * Centers the camera in the specified rectangle.
     *
     * @param x0
     *         The left side.
     * @param y0
     *         The bottom side.
     * @param x1
     *         The right side.
     * @param y1
     *         The top side.
     */
    public void centerIn(float x0, float y0, float x1, float y1)
    {
        float diffX = x1 - x0;
        float diffY = y1 - y0;
        position.set(x0 + diffX * .5f, y0 + diffY * .5f, 0f);
        update();
    }

    /**
     * Centers the camera in the specified rectangle.
     *
     * @param rectangle
     *         The rectangle.
     */
    public void centerIn(Rectangle rectangle)
    {
        centerIn(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height);
    }

    /**
     * Sets the unit viewport based on the current screen size.
     *
     * @param width
     *         The screen w.
     * @param height
     *         The screen h.
     * @param keepAspectRatio
     *         true to keep the aspect ratio; false, otherwise.
     */
    public void setUnitViewport(int width, int height, boolean keepAspectRatio)
    {
        float aspectRatio = (float) height / (float) width;
        float unitWidth = Units.instance.getUnits(width);
        float unitHeight = Units.instance.getUnits(height);
        if(keepAspectRatio)
        {
            unitHeight = unitWidth * aspectRatio;
        }
        viewportWidth = unitWidth;
        viewportHeight = unitHeight;
        update();
    }
}
