/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.scenes;

import com.badlogic.gdx.utils.Disposable;

/** Defines a game scene. */
public interface Scene extends Disposable {
    /** Draws the scene. */
    void draw();

    /**
     * Gets the current scene state.
     *
     * @return The current {@link SceneState}.
     */
    SceneState getState();

    /** Hides the scene. */
    void hide();

    /**
     * Determines if the scene is an overlay.  An overlay scene will not cause a scene below it to be automatically
     * removed from the scene stack.
     *
     * @return true if the scene is an overlay; otherwise, false.
     */
    boolean isOverlay();

    /** Notifies the scene to pause. */
    void pause();

    /**
     * Notifies the scene of a resolution change.
     *
     * @param width
     *         The new window w (in pixels).
     * @param height
     *         The new window h (in pixels).
     */
    void resize(int width, int height);

    /** Notifies the scene to resume after a pause. */
    void resume();

    /** Shows the scene. */
    void show();

    /**
     * Updates the scene.
     *
     * @param tt
     *         The total amount of time (in seconds) that has elapsed since the game began.
     * @param dt
     *         The amount of time (in seconds) that has elapsed since the previous frame.
     */
    void update(double tt, double dt);
}
