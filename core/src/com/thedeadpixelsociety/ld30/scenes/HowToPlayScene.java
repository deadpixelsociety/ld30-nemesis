package com.thedeadpixelsociety.ld30.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.thedeadpixelsociety.ld30.GameServices;
import com.thedeadpixelsociety.ld30.assets.AssetService;

public class HowToPlayScene extends SceneAdapter
{
    private Sound buttonClickSound;
    private BitmapFont font;
    private Stage stage;
    private Viewport viewport;

    public HowToPlayScene(final GameServices gameServices)
    {
        super(gameServices);
    }

    @Override public void dispose()
    {
        stage.dispose();
        font.dispose();
        buttonClickSound.dispose();
    }

    @Override public void draw()
    {
        stage.draw();
    }

    @Override public void resize(final int width, final int height)
    {
        viewport.setScreenSize(width, height);
        viewport.apply(true);
    }

    @Override public void show()
    {
        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport);

        buttonClickSound = Gdx.audio.newSound(Gdx.files.internal("audio/button-click.wav"));

        final Table table = new Table();
        table.setFillParent(true);

        table.row()
             .center()
             .expandX();

        final FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/JLSDataGothicC_NC" +
                                                                                             ".otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 32;

        font = generator.generateFont(parameter);

        generator.dispose();

        final Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.WHITE);

        final String text = "You are the malevolent, sentient planet Nemesis.\n" +
                            "Since the begin of your long and destructive life\n" +
                            "you have desired only one thing...\n" +
                            "The destruction of all other planets in the universe.\n" +
                            "Using your innate ability to create a gravity tether\n" + "" +
                            "between other planets and send them hurtling across\n" +
                            "space you seek out inhabited solar systems and\n" +
                            "wreak your evil havoc.\n\n" +
                            "Click left and hold to move.\n" +
                            "Click left and hold on a planet to tether to it.\n" +
                            "Swing while tethered to crash the planet into others.\n" +
                            "Release left to release the tether.\n" +
                            "Try swinging and releasing!\n";
        final Label howToPlayLabel = new Label(text, labelStyle);
        howToPlayLabel.setWrap(false);

        table.add(howToPlayLabel);
        table.row()
             .center()
             .padTop(32f);

        final AssetService assetService = gameServices.get(AssetService.class);
        final TextureAtlas ui = assetService.get("ui", TextureAtlas.class);

        final Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
        buttonStyle.up = new SpriteDrawable(ui.createSprite("button-back-unclicked"));
        buttonStyle.down = new SpriteDrawable(ui.createSprite("button-back-clicked"));

        final Button backButton = new Button(buttonStyle);
        backButton.addListener(new ClickListener(Input.Buttons.LEFT)
        {
            @Override public boolean touchDown(final InputEvent event,
                                               final float x,
                                               final float y,
                                               final int pointer,
                                               final int button)
            {
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override public void clicked(final InputEvent event, final float x, final float y)
            {
                buttonClickSound.play(.3f);
                gameServices.get(SceneService.class)
                            .add(new MainMenuScene(gameServices));
            }
        });

        table.add(backButton);

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override public void update(final double tt, final double dt)
    {
        stage.act((float) dt);
    }
}
