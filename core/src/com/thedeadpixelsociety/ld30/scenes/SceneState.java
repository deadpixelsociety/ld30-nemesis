/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.scenes;

/** Enumerates the available scene states. */
public enum SceneState {
    /** The scene is currently active. */
    ACTIVE,
    /** The scene is inactive and should be removed. */
    INACTIVE,
}
