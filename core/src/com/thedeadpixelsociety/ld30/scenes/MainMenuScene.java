package com.thedeadpixelsociety.ld30.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.thedeadpixelsociety.ld30.GameServices;
import com.thedeadpixelsociety.ld30.assets.AssetService;

public class MainMenuScene extends SceneAdapter
{
    private Sound buttonClickSound;
    private OrthographicCamera camera = new OrthographicCamera();
    private Music music;
    private Stage stage;
    private Viewport viewport;

    public MainMenuScene(final GameServices gameServices)
    {
        super(gameServices);
    }

    @Override public void dispose()
    {
        stage.dispose();

        if(music.isPlaying())
        {
            music.stop();
        }

        music.dispose();

        buttonClickSound.dispose();
    }

    @Override public void draw()
    {
        stage.draw();
    }

    @Override public void resize(final int width, final int height)
    {
        viewport.update(width, height);
        viewport.apply(true);
    }

    @Override public void show()
    {
        music = Gdx.audio.newMusic(Gdx.files.internal("audio/music.mp3"));
        music.setVolume(.1f);
        music.setLooping(true);
        music.play();

        buttonClickSound = Gdx.audio.newSound(Gdx.files.internal("audio/button-click.wav"));

        stage = new Stage();
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport.apply(true);
        stage.setViewport(viewport);

        final AssetService assetService = gameServices.get(AssetService.class);
        final TextureAtlas ui = assetService.get("ui", TextureAtlas.class);

        final Image titleScreen = new Image(ui.createSprite("title-screen"));
        //titleScreen.setCenterPosition(0f, 0f);

        final Table table = new Table();
        table.setFillParent(true);
        final Group buttons = new HorizontalGroup();

        final Button.ButtonStyle playButtonStyle = new Button.ButtonStyle();
        playButtonStyle.up = new SpriteDrawable(ui.createSprite("button-play-unclicked"));
        playButtonStyle.down = new SpriteDrawable(ui.createSprite("button-play-clicked"));

        final Button playButton = new Button(playButtonStyle);

        playButton.addListener(new ClickListener(Input.Buttons.LEFT)
        {
            @Override public void clicked(final InputEvent event, final float x, final float y)
            {
                buttonClickSound.play(.3f);
                gameServices.get(SceneService.class)
                            .add(new GameScene(gameServices));
            }
        });

        final Button.ButtonStyle howToButtonStyle = new Button.ButtonStyle();
        howToButtonStyle.up = new SpriteDrawable(ui.createSprite("button-howtoplay-unclicked"));
        howToButtonStyle.down = new SpriteDrawable(ui.createSprite("button-howtoplay-clicked"));

        final Button howToButton = new Button(howToButtonStyle);

        howToButton.addListener(new ClickListener(Input.Buttons.LEFT)
        {
            @Override public void clicked(final InputEvent event, final float x, final float y)
            {
                buttonClickSound.play(.3f);
                gameServices.get(SceneService.class)
                            .add(new HowToPlayScene(gameServices));
            }
        });

        final Button.ButtonStyle quitButtonStyle = new Button.ButtonStyle();
        quitButtonStyle.up = new SpriteDrawable(ui.createSprite("button-quit-unclicked"));
        quitButtonStyle.down = new SpriteDrawable(ui.createSprite("button-quit-clicked"));

        final Button quitButton = new Button(quitButtonStyle);

        quitButton.addListener(new ClickListener(Input.Buttons.LEFT)
        {
            @Override public void clicked(final InputEvent event, final float x, final float y)
            {
                buttonClickSound.play(.3f   );
                Gdx.app.exit();
            }
        });

        buttons.addActor(playButton);
        buttons.addActor(howToButton);
        buttons.addActor(quitButton);

        table.bottom();
        table.padBottom(32f);
        table.row()
             .bottom()
             .fill();
        table.add(buttons);

        stage.addActor(titleScreen);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override public void update(final double tt, final double dt)
    {
        stage.act((float) dt);
    }
}
