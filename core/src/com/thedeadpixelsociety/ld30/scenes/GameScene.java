package com.thedeadpixelsociety.ld30.scenes;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.thedeadpixelsociety.ld30.*;
import com.thedeadpixelsociety.ld30.celestial.*;
import com.thedeadpixelsociety.ld30.components.*;
import com.thedeadpixelsociety.ld30.graphics.Camera2D;
import com.thedeadpixelsociety.ld30.graphics.SpriteBundles;
import com.thedeadpixelsociety.ld30.systems.*;

public class GameScene extends SceneAdapter
{
    private static final int MAX_PLANETS = 21;
    private static final int MIN_PLANETS = 7;
    private static final Array<String> stars = new Array<String>()
    {
        { add("one"); }

        { add("two");}

        {add("three");}
    };
    private final Array<Entity> entitiesToDelete = new Array<Entity>();
    private final Array<Sound> explosionSounds = new Array<Sound>();
    private int backgroundCacheId;
    private Sound bumpSound;
    private Camera2D camera;
    private int damageKills;
    private float dir = 1f;
    private Sound explosionSound1;
    private Sound explosionSound2;
    private Sound explosionSound3;
    private BitmapFont fontLarge;
    private BitmapFont fontSmall;
    private BitmapFont fontTiny;
    private Array<FragmentToCreate> fragmentsToCreate = new Array<FragmentToCreate>();
    private boolean gameReady;
    private Sprite healthBar;
    private Sprite healthGauge;
    private float healthRemaining;
    private Rectangle maxBounds = new Rectangle();
    private Music music;
    private boolean paused;
    private int planetsRemaining;
    private boolean showSystemName;
    private SolarSystem solarSystem;
    private int starKills;
    private boolean tethered;
    private Body tetheredBody;
    private float timer;
    private OrthographicCamera uiCamera;
    private long victims;
    private final HealthSystem.HealthEventListener healthEventListener = new HealthSystem.HealthEventListener()
    {
        @Override public boolean onDamaged(final Entity entity, final Health health, final float amount)
        {
            return false;
        }

        @Override public boolean onHealed(final Entity entity, final Health health, final float amount)
        {
            return false;
        }

        @Override public boolean onKilled(final Entity entity, final Health health, final int source)
        {
            final Tag tag = entity.getComponent(Tag.class);
            if(tag != null)
            {
                final Physics physics = entity.getComponent(Physics.class);
                if(physics != null)
                {
                    if(tag.getTags()
                          .contains(Tags.PLANETS) ||
                       tag.getTags()
                          .contains(Tags.PLAYER))
                    {
                        final ParticleEffect effect = world.getSystem(ParticleSystem.class)
                                                           .createEffect(Emits.ParticleType.EXPLOSION);

                        final Vector2 position = physics.getBody()
                                                        .getPosition();

                        effect.setPosition(position.x, position.y);
                        effect.start();

                        world.getSystem(ParticleSystem.class)
                             .addUnattachedEffect(Emits.ParticleType.EXPLOSION,
                                                  (ParticleEffectPool.PooledEffect) effect);

                        explosionSounds.random()
                                       .play(.3f);

                        final Demographics demographics = entity.getComponent(Demographics.class);
                        if(demographics != null &&
                           tag.getTags()
                              .contains(Tags.PLANETS))
                        {
                            victims += demographics.getPopulation();

                            final PlanetSize planetSize = demographics.getSize();

                            int numFragments = 0;
                            switch(planetSize)
                            {
                                case GIANT:
                                    numFragments = MathUtils.random(16, 32);
                                    break;
                                case LARGE:
                                    numFragments = MathUtils.random(8, 16);
                                    break;
                                case MEDIUM:
                                    numFragments = MathUtils.random(4, 8);
                                    break;
                                case SMALL:
                                    numFragments = MathUtils.random(2, 4);
                                    break;
                            }

                            for(int i = 0; i < numFragments; i++)
                            {

                                final Vector2 fragmentPos = new Vector2(1f, 0f).setAngle(MathUtils.random() * 360f)
                                                                               .nor()
                                                                               .scl(MathUtils.random() * 2f)
                                                                               .add(position);

                                final FragmentToCreate fragmentToCreate = new FragmentToCreate();
                                fragmentToCreate.planetType = demographics.getType();
                                fragmentToCreate.position.set(fragmentPos);
                                fragmentToCreate.center.set(position);

                                fragmentsToCreate.add(fragmentToCreate);
                            }
                        }

                        if(tag.getTags()
                              .contains(Tags.PLAYER))
                        {
                            gameOver();
                        }
                        else
                        {
                            if(source == DeathSources.STAR)
                            {
                                starKills++;
                            }
                            else
                            {
                                damageKills++;
                            }
                        }
                    }
                    else if(tag.getTags()
                               .contains(Tags.FRAGMENTS))
                    {
                        final ParticleEffect effect = world.getSystem(ParticleSystem.class)
                                                           .createEffect(Emits.ParticleType.EXPLOSION_SMALL);

                        final Vector2 position = physics.getBody()
                                                        .getPosition();

                        effect.setPosition(position.x, position.y);
                        effect.start();

                        world.getSystem(ParticleSystem.class)
                             .addUnattachedEffect(Emits.ParticleType.EXPLOSION_SMALL,
                                                  (ParticleEffectPool.PooledEffect) effect);

                    }
                }
            }

            if(!entitiesToDelete.contains(entity, true))
            {
                entitiesToDelete.add(entity);
            }

            return false;
        }

        @Override public boolean onRestored(final Entity entity, final Health health)
        {
            return false;
        }
    };
    private boolean wasClicking;
    private World world;

    /**
     * Constructs a {@link com.thedeadpixelsociety.ld30.scenes.SceneAdapter}.
     *
     * @param gameServices
     *         The game services container.
     */
    public GameScene(final GameServices gameServices)
    {
        super(gameServices);
    }

    @Override public void dispose()
    {
        world.getSystem(PhysicsSystem.class)
             .dispose();

        if(music.isPlaying())
        {
            music.stop();
        }

        music.dispose();

        bumpSound.dispose();

        explosionSound3.dispose();
        explosionSound2.dispose();
        explosionSound1.dispose();

        fontSmall.dispose();
        fontTiny.dispose();
        fontLarge.dispose();
    }

    @Override public void draw()
    {
        Gdx.gl.glClearColor(.05f, .05f, .05f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        final SpriteCache spriteCache = gameServices.get(SpriteCache.class);

        spriteCache.setProjectionMatrix(camera.combined);
        spriteCache.begin();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteCache.draw(backgroundCacheId);
        spriteCache.end();

        world.getSystem(RenderSystem.class)
             .process();

        /*
        world.getSystem(PhysicsSystem.class)
             .renderDebug(camera.combined);
             */

        final SpriteBatch spriteBatch = gameServices.get(SpriteBatch.class);

        spriteBatch.setProjectionMatrix(uiCamera.combined);
        spriteBatch.begin();

        drawPlanetNames(spriteBatch);

        if(showSystemName)
        {
            drawSystemTitle(spriteBatch);
        }
        else
        {
            drawPlanetRemaining(spriteBatch);
            drawSystemNameUI(spriteBatch);

            drawHealthBar(spriteBatch);
        }

        spriteBatch.end();
    }

    @Override public void pause()
    {
        paused = true;
    }

    @Override public void resize(final int width, final int height)
    {
        uiCamera.setToOrtho(false, width, height);
        uiCamera.update();
    }

    @Override public void resume()
    {
        paused = false;
    }

    @Override public void show()
    {
        music = Gdx.audio.newMusic(Gdx.files.internal("audio/music.mp3"));
        music.setVolume(.1f);
        music.setLooping(true);
        music.play();

        bumpSound = Gdx.audio.newSound(Gdx.files.internal("audio/bump.wav"));
        explosionSound1 = Gdx.audio.newSound(Gdx.files.internal("audio/explosion1.wav"));
        explosionSound2 = Gdx.audio.newSound(Gdx.files.internal("audio/explosion2.wav"));
        explosionSound3 = Gdx.audio.newSound(Gdx.files.internal("audio/explosion3.wav"));

        explosionSounds.add(explosionSound1);
        explosionSounds.add(explosionSound2);
        explosionSounds.add(explosionSound3);

        camera = gameServices.get(Camera2D.class);
        camera.zoom = Camera2D.MIN_ZOOM;

        uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        healthGauge = SpriteBundles.instance.getSprite("healthbar");
        healthBar = SpriteBundles.instance.getSprite("healthbar-fill");

        createFonts();

        solarSystem = new SolarSystem(MIN_PLANETS, MAX_PLANETS);

        createWorld();

        solarSystem.generate(world);

        calculateMaxZoom();

        createBackground();

        createPlayer();

        showSystemName = true;

        Timer.schedule(new Timer.Task()
        {
            @Override public void run()
            {
                final Entity player = world.getSystem(TagSystem.class)
                                           .getFirst(Tags.PLAYER);
                final Body playerBody = player.getComponent(Physics.class)
                                              .getBody();

                world.getSystem(CameraSystem.class)
                     .panTo(playerBody.getPosition(), 2f);
                world.getSystem(CameraSystem.class)
                     .smoothZoom(Camera2D.MIN_ZOOM, 2f);

                Timer.schedule(new Timer.Task()
                {
                    @Override public void run()
                    {
                        gameReady = true;
                    }
                }, 2.1f);
            }
        }, 4f);
    }

    @Override public void update(final double tt, final double dt)
    {
        if(!paused)
        {
            updateSystemNameFade(dt);

            fragmentsToCreate.clear();

            updatePlayer();

            entitiesToDelete.clear();

            world.setDelta((float) dt);
            world.process();

            for(FragmentToCreate fragmentToCreate : fragmentsToCreate)
            {
                final Entity fragment = EntityFactory.createFragment(world,
                                                                     fragmentToCreate.planetType,
                                                                     fragmentToCreate.position.x,
                                                                     fragmentToCreate.position.y);

                final Physics fragmentPhysics = fragment.getComponent(Physics.class);

                final Vector2 force = fragmentToCreate.position.sub(fragmentToCreate.center)
                                                               .nor()
                                                               .scl(200f);
                fragmentPhysics.getBody()
                               .setLinearVelocity(force.x, force.y);
            }

            for(Entity entity : entitiesToDelete)
            {
                final Tag tag = entity.getComponent(Tag.class);
                if(tag != null &&
                   tag.getTags()
                      .contains(Tags.TETHERED_PLANET))
                {
                    tethered = false;
                    tetheredBody = null;

                    world.getSystem(CameraSystem.class)
                         .smoothZoom(Camera2D.MIN_ZOOM, .5f);
                }

                world.deleteEntity(entity);
            }

            planetsRemaining = world.getSystem(TagSystem.class)
                                    .getAll(Tags.PLANETS).size;

            if(planetsRemaining <= 0)
            {
                gameOver();
            }
        }
    }

    private void calculateMaxZoom()
    {
        final Rectangle bounds = solarSystem.getBounds();

        Vector3 topLeft = new Vector3(bounds.x, bounds.y + bounds.height, 0f);
        Vector3 topRight = new Vector3(bounds.x + bounds.width, bounds.y + bounds.height, 0f);
        Vector3 bottomLeft = new Vector3(bounds.x, bounds.y, 0f);
        Vector3 bottomRight = new Vector3(bounds.x + bounds.width, bounds.y, 0f);

        final float top = camera.viewportHeight;
        final float right = camera.viewportWidth;
        final float left = 0f;
        final float bottom = 0f;

        camera.project(topLeft, left, bottom, right, top);
        camera.project(topRight, left, bottom, right, top);
        camera.project(bottomLeft, left, bottom, right, top);
        camera.project(bottomRight, left, bottom, right, top);

        while((topLeft.x < left || topLeft.y > top) ||
              (topRight.x > right || topRight.y > top) ||
              (bottomLeft.x < left || bottomLeft.y < bottom) ||
              (bottomRight.x > right || bottomRight.y < bottom))
        {
            camera.zoom += .01f;
            camera.update();

            topLeft = new Vector3(bounds.x, bounds.y + bounds.height, 0f);
            topRight = new Vector3(bounds.x + bounds.width, bounds.y + bounds.height, 0f);
            bottomLeft = new Vector3(bounds.x, bounds.y, 0f);
            bottomRight = new Vector3(bounds.x + bounds.width, bounds.y, 0f);

            camera.project(topLeft, left, bottom, right, top);
            camera.project(topRight, left, bottom, right, top);
            camera.project(bottomLeft, left, bottom, right, top);
            camera.project(bottomRight, left, bottom, right, top);
        }

        maxBounds.set(topLeft.x, bottomLeft.y, topRight.x - topLeft.x, topLeft.y - bottomLeft.y);
    }

    private void createBackground()
    {
        final float tileSize = 16f;

        final Rectangle bounds = solarSystem.getBounds();

        final float cols = MathUtils.ceil(bounds.width / tileSize) + 2;
        final float rows = MathUtils.ceil(bounds.height / tileSize) + 2;

        final float bgWidth = cols * tileSize;
        final float bgHeight = rows * tileSize;

        final float xDiff = (bgWidth - bounds.width) * .5f;
        final float yDiff = (bgHeight - bounds.height) * .5f;

        float x = bounds.x - xDiff;
        float y = bounds.y - yDiff;

        final SpriteCache spriteCache = new SpriteCache((int) (rows * cols), false);
        gameServices.put(spriteCache);

        spriteCache.setProjectionMatrix(camera.combined);
        spriteCache.beginCache();

        for(int j = 0; j < (int) rows; j++)
        {
            for(int i = 0; i < (int) cols; i++)
            {
                final float xx = x + tileSize * i;
                final float yy = y + tileSize * j;

                final Sprite sprite = SpriteBundles.instance.getSprite("star-" + stars.random());
                sprite.setPosition(xx - sprite.getWidth() * .5f, yy - sprite.getHeight() * .5f);
                spriteCache.add(sprite);
            }
        }

        backgroundCacheId = spriteCache.endCache();
    }

    private void createFonts()
    {
        final FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/JLSDataGothicC_NC" +
                                                                                             ".otf"));

        final FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 64;

        fontLarge = generator.generateFont(parameter);

        parameter.size = 24;
        fontSmall = generator.generateFont(parameter);

        parameter.size = 16;
        fontTiny = generator.generateFont(parameter);

        generator.dispose();
    }

    private void createPlayer()
    {
        final int side = MathUtils.random(3);
        final Rectangle bounds = solarSystem.getBounds();
        final Vector2 position = new Vector2();

        switch(side)
        {
            // top
            case 0:
                position.set(MathUtils.random(-bounds.width * .5f, bounds.width * .5f), bounds.y + bounds.height);
                break;
            // right
            case 1:
                position.set(bounds.x + bounds.width, MathUtils.random(-bounds.height * .5f, bounds.height * .5f));
                break;
            // left
            case 2:
                position.set(bounds.x, MathUtils.random(-bounds.height * .5f, bounds.height * .5f));
                break;
            // bottom
            case 3:
                position.set(MathUtils.random(-bounds.width * .5f, bounds.width * .5f), bounds.y);
                break;
        }

        EntityFactory.createPlayer(world, position.x, position.y);
    }

    private void createWorld()
    {
        world = new World();
        world.setSystem(new TagSystem());
        world.setSystem(new PhysicsSystem(solarSystem));
        world.setSystem(new CameraSystem(camera));
        world.setSystem(new HealthSystem());
        world.setSystem(new ParticleSystem());
        world.setSystem(new RenderSystem(gameServices.get(SpriteBatch.class)), true);

        world.initialize();

        world.getSystem(HealthSystem.class)
             .addListener(healthEventListener);
    }

    private void drawHealthBar(final SpriteBatch spriteBatch)
    {
        healthBar.setPosition(16f, Gdx.graphics.getHeight() - 40f - healthBar.getHeight());
        healthGauge.setPosition(16f, Gdx.graphics.getHeight() - 40f - healthGauge.getHeight());

        final Entity player = world.getSystem(TagSystem.class)
                                   .getFirst(Tags.PLAYER);
        Color color = Color.WHITE;
        if(player != null)
        {
            color = world.getSystem(HealthSystem.class)
                         .getHealthColor(player);
        }

        healthBar.draw(spriteBatch);
        healthBar.setColor(color);
        healthGauge.draw(spriteBatch);
    }

    private void drawPlanetNames(final SpriteBatch spriteBatch)
    {
        for(Entity planet : solarSystem.getPlanets())
        {
            if(planet != null)
            {
                final Physics physics = planet.getComponent(Physics.class);
                final Demographics demographics = planet.getComponent(Demographics.class);
                final Dimensions dimensions = planet.getComponent(Dimensions.class);
                if(physics != null && demographics != null && dimensions != null)
                {
                    Color color = world.getSystem(HealthSystem.class)
                                       .getHealthColor(planet);
                    final float radius = dimensions.getRadius();
                    final Vector3 planetPos = new Vector3(physics.getBody()
                                                                 .getPosition(), 0f);
                    final Vector3 topLeft = new Vector3(planetPos.x - radius, planetPos.y + radius, 0f);
                    final Vector3 topRight = new Vector3(planetPos.x + radius, planetPos.y + radius, 0f);
                    final Vector3 bottomLeft = new Vector3(planetPos.x - radius, planetPos.y - radius, 0f);
                    final Vector3 bottomRight = new Vector3(planetPos.x + radius, planetPos.y - radius, 0f);

                    camera.project(topLeft, 0f, 0f, camera.viewportWidth, camera.viewportHeight);
                    camera.project(topRight, 0f, 0f, camera.viewportWidth, camera.viewportHeight);
                    camera.project(bottomLeft, 0f, 0f, camera.viewportWidth, camera.viewportHeight);
                    camera.project(bottomRight, 0f, 0f, camera.viewportWidth, camera.viewportHeight);

                    final Rectangle planetBounds = new Rectangle(bottomLeft.x,
                                                                 bottomLeft.y,
                                                                 topRight.x - topLeft.x,
                                                                 topRight.y - bottomRight.y);

                    final Rectangle cameraBounds = new Rectangle(0f, 0f, camera.viewportWidth, camera.viewportHeight);

                    final String name = demographics.getName();
                    final BitmapFont.TextBounds bounds = fontTiny.getBounds(name);
                    final Vector2 bodyPosition = physics.getBody()
                                                        .getPosition();

                    if(cameraBounds.overlaps(planetBounds) || cameraBounds.contains(planetBounds))
                    {
                        final Vector3 offset = new Vector3(bodyPosition.x, bodyPosition.y + radius, 0f);

                        camera.project(offset);

                        float x = offset.x - bounds.width * .5f;
                        float y = offset.y + bounds.height + 8f;

                        fontTiny.setColor(color);
                        fontTiny.draw(spriteBatch, name, x, y);
                    }
                    else if(gameReady)
                    {
                        final Entity player = world.getSystem(TagSystem.class)
                                                   .getFirst(Tags.PLAYER);
                        if(player != null)
                        {
                            final Vector2 playerPos = player.getComponent(Physics.class)
                                                            .getBody()
                                                            .getPosition();

                            final Vector2 dir = bodyPosition.sub(playerPos.x, playerPos.y)
                                                            .nor();

                            final float sw = Gdx.graphics.getWidth();
                            final float sh = Gdx.graphics.getHeight();

                            final Vector2 textPos = new Vector2();
                            if(Math.abs(dir.x) > Math.abs(dir.y))
                            {
                                if(dir.x < 0f)
                                {
                                    textPos.set(8f, (sh * .5f) + (dir.y * (sh * .5f)));
                                }
                                else if(dir.x > 0f)
                                {
                                    textPos.set(sw - 8f - bounds.width, (sh * .5f) + (dir.y * (sh * .5f)));
                                }
                            }
                            else
                            {
                                if(dir.y < 0f)
                                {
                                    textPos.set((sw * .5f) + (dir.x * (sw * .5f)), bounds.height + 8f);
                                }
                                else if(dir.y > 0f)
                                {
                                    textPos.set((sw * .5f) + (dir.x * (sw * .5f)), sh - 8f);
                                }
                            }

                            fontTiny.setColor(color);
                            fontTiny.draw(spriteBatch, name, textPos.x, textPos.y);
                        }
                    }
                }
            }
        }
    }

    private void drawPlanetRemaining(final SpriteBatch spriteBatch)
    {
        final String population = "Planets Remaining";
        final BitmapFont.TextBounds populationBounds = fontSmall.getBounds(population);
        float x = Gdx.graphics.getWidth() - populationBounds.width - 16f;
        float y = Gdx.graphics.getHeight() - 16f;
        fontSmall.draw(spriteBatch, population, x, y);

        final String count = Integer.toString(planetsRemaining);
        final BitmapFont.TextBounds countBounds = fontSmall.getBounds(count);
        x = Gdx.graphics.getWidth() - countBounds.width - 16f;
        y = Gdx.graphics.getHeight() - populationBounds.height - 32f;
        fontSmall.draw(spriteBatch, count, x, y);
    }

    private void drawSystemNameUI(final SpriteBatch spriteBatch)
    {
        final String systemName = "The " + solarSystem.getName() + " System";
        float x = 16f;
        float y = Gdx.graphics.getHeight() - 16f;
        fontSmall.draw(spriteBatch, systemName, x, y);
    }

    private void drawSystemTitle(final SpriteBatch spriteBatch)
    {
        float alpha = Interpolation.sineIn.apply(timer / 4f);

        if(dir == -1f)
        {
            alpha = 1f - Interpolation.sineIn.apply(timer / 2f);
        }

        final Color titleColor = new Color(Color.WHITE);
        titleColor.a = alpha;

        final String name = "The " + solarSystem.getName() + " System";
        final BitmapFont.TextBounds bounds = fontLarge.getBounds(name);
        float x = (Gdx.graphics.getWidth() - bounds.width) * .5f;
        float y = (Gdx.graphics.getHeight() - bounds.height) * .5f;
        fontLarge.setColor(titleColor);
        fontLarge.draw(spriteBatch, name, x, y + bounds.height);
    }

    private void gameOver()
    {
        final Entity player = world.getSystem(TagSystem.class)
                                   .getFirst(Tags.PLAYER);
        if(player != null)
        {
            final Health health = player.getComponent(Health.class);
            healthRemaining = health.getHealth() / health.getMaxHealth();
        }

        Timer.schedule(new Timer.Task()
        {
            @Override public void run()
            {
                gameServices.get(SceneService.class)
                            .add(new GameOverScene(gameServices, starKills, damageKills, victims, healthRemaining));

                paused = true;
            }
        }, 2f);
    }

    private void updatePlayer()
    {
        final Entity player = world.getSystem(TagSystem.class)
                                   .getFirst(Tags.PLAYER);

        if(gameReady && player != null)
        {
            final Body playerBody = player.getComponent(Physics.class)
                                          .getBody();

            if(Gdx.input.isButtonPressed(Input.Buttons.LEFT))
            {
                if(!tethered && !wasClicking)
                {
                    final Vector3 mousePos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0f);
                    camera.unproject(mousePos);

                    final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);
                    physicsSystem.query(new QueryCallback()
                    {
                        @Override public boolean reportFixture(final Fixture fixture)
                        {
                            final Body body = fixture.getBody();
                            final Object userData = body.getUserData();
                            if(userData instanceof Entity)
                            {
                                Entity entity = (Entity) userData;
                                final Tag tag = entity.getComponent(Tag.class);
                                if(tag != null &&
                                   tag.getTags()
                                      .contains(Tags.PLANETS))
                                {
                                    tetheredBody = body;
                                    return false;
                                }
                            }

                            return true;
                        }
                    }, mousePos.x - 1f, mousePos.y - 1f, 2f, 2f);

                    if(tetheredBody != null)
                    {
                        for(JointEdge jointEdge : tetheredBody.getJointList())
                        {
                            physicsSystem.destroyJoint(jointEdge.joint);
                        }

                        final Entity planet = (Entity) tetheredBody.getUserData();

                        tetheredBody.setAngularDamping(.9f);
                        tetheredBody.setLinearDamping(.9f);
                        tetheredBody.getFixtureList()
                                    .get(0)
                                    .setDensity(0f);
                        tetheredBody.resetMassData();

                        final float radius1 = planet.getComponent(Dimensions.class)
                                                    .getRadius();
                        final float radius2 = player.getComponent(Dimensions.class)
                                                    .getRadius();

                        final float maxLength = (radius1 + radius2) * 2.75f;

                        physicsSystem.createRopeJoint(playerBody, tetheredBody, maxLength);

                        if(tetheredBody.getUserData() instanceof Entity)
                        {
                            final Entity tetheredEntity = (Entity) tetheredBody.getUserData();
                            world.getSystem(TagSystem.class)
                                 .tag(tetheredEntity, Tags.TETHERED_PLANET);
                        }

                        tethered = true;

                        world.getSystem(CameraSystem.class)
                             .smoothZoom(Camera2D.MIN_ZOOM * 1.5f, 1f);
                    }
                }

                Vector3 worldMousePos = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0f));

                Vector2 mousePos = new Vector2(worldMousePos.x, worldMousePos.y);
                Vector2 playerPos = new Vector2(playerBody.getPosition().x, playerBody.getPosition().y);

                Vector2 diff = mousePos.sub(playerPos);
                Vector2 dir = new Vector2();
                if(diff.len() <= .3f)
                {
                    dir.set(0f, 0f);
                }
                else
                {
                    dir.set(diff.nor());
                }

                playerBody.setLinearVelocity(dir.x * 100f, dir.y * 100f);

                if(!dir.epsilonEquals(Vector2.Zero, .01f))
                {
                    world.getSystem(CameraSystem.class)
                         .track(playerBody.getPosition());
                }

                wasClicking = true;
            }
            else
            {
                playerBody.setLinearVelocity(0f, 0f);

                if(tethered && tetheredBody != null)
                {
                    for(JointEdge jointEdge : tetheredBody.getJointList())
                    {
                        world.getSystem(PhysicsSystem.class)
                             .destroyJoint(jointEdge.joint);
                    }

                    tetheredBody.setLinearDamping(.3f);
                    tetheredBody.setAngularDamping(.3f);
                    tetheredBody.getFixtureList()
                                .get(0)
                                .setDensity(1f);
                    tetheredBody.resetMassData();

                    final Entity entity = (Entity) tetheredBody.getUserData();
                    world.getSystem(TagSystem.class)
                         .untag(entity, Tags.TETHERED_PLANET);

                    tethered = false;
                    tetheredBody = null;

                    world.getSystem(CameraSystem.class)
                         .smoothZoom(Camera2D.MIN_ZOOM, 1f);
                }

                wasClicking = false;
            }
        }
    }

    private void updateSystemNameFade(final double dt)
    {
        if(showSystemName)
        {
            timer += dt;
            if(dir == 1f)
            {
                if(timer >= 4f)
                {
                    timer = 0f;
                    dir = -1f;
                }
            }
            else
            {
                if(timer >= 2f)
                {
                    showSystemName = false;
                }
            }
        }
    }

    private class FragmentToCreate
    {
        public final Vector2 center = new Vector2();
        public final Vector2 position = new Vector2();
        public PlanetType planetType;
    }
}
