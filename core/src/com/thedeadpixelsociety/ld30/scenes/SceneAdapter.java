/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.scenes;

import com.thedeadpixelsociety.ld30.GameServices;

/** Implements the {@link Scene} contract as an empty abstract class. */
public abstract class SceneAdapter implements Scene {
    protected final GameServices gameServices;
    protected SceneState sceneState = SceneState.ACTIVE;

    /**
     * Constructs a {@link com.thedeadpixelsociety.like.scenes.SceneAdapter}.
     *
     * @param gameServices
     *         The game services container.
     */
    public SceneAdapter(GameServices gameServices) {
        this.gameServices = gameServices;
    }

    /**
     * Gets the current scene state.
     *
     * @return The current {@link com.thedeadpixelsociety.like.scenes.SceneState}.
     */
    @Override
    public SceneState getState() {
        return sceneState;
    }

    /**
     * Determines if the scene is an overlay.  An overlay scene will not cause a scene below it to be automatically
     * removed from the scene stack.
     *
     * @return true if the scene is an overlay; otherwise, false.
     */
    @Override
    public boolean isOverlay() {
        return false;
    }

    /** Releases all resources of this object. */
    @Override
    public void dispose() {
    }

    /** Draws the scene. */
    @Override
    public void draw() {
    }

    /** Hides the scene. */
    @Override
    public void hide() {
    }

    /** Notifies the scene to pause. */
    @Override
    public void pause() {
    }

    /**
     * Notifies the scene of a resolution change.
     *
     * @param width
     *         The new window w (in pixels).
     * @param height
     *         The new window h (in pixels).
     */
    @Override
    public void resize(int width, int height) {
    }

    /** Notifies the scene to resume after a pause. */
    @Override
    public void resume() {
    }

    /** Shows the scene. */
    @Override
    public void show() {
    }

    /**
     * Updates the scene.
     *
     * @param tt
     *         The total amount of time (in seconds) that has elapsed since the game began.
     * @param dt
     *         The amount of time (in seconds) that has elapsed since the previous frame.
     */
    @Override
    public void update(double tt, double dt) {
    }
}
