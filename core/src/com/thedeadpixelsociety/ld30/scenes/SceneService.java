/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public final class SceneService implements Disposable {
    // Private Fields
    private final Array<Scene> scenes = new Array<Scene>();

    /** Releases all resources of this object. */
    @Override
    public void dispose() {
        for(Scene scene : scenes) {
            if(scene != null) {
                scene.hide();
                scene.dispose();
            }
        }
        scenes.clear();
    }

    /**
     * Removes the specified scene.
     *
     * @param index
     *         The scene index.
     */
    private void remove(int index) {
        Scene scene = scenes.removeIndex(index);
        scene.hide();
        scene.dispose();
    }

// Methods

    /**
     * Adds the specified scene.
     *
     * @param scene
     *         The scene.
     */
    public void add(Scene scene) {
        if(scene == null) throw new IllegalArgumentException("Scene cannot be null.");
        scene.show();
        scene.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        scenes.add(scene);
    }

    /** Draws all active scenes. */
    public void draw() {
        // Draw from the bottom up to allow overlay transparency.
        for(Scene scene : scenes) {
            if(scene != null && scene.getState().equals(SceneState.ACTIVE)) {
                scene.draw();
            }
        }
    }

    /** Notifies all scenes to pause. */
    public void pause() {
        for(Scene scene : scenes) {
            if(scene != null) {
                scene.pause();
            }
        }
    }

    /**
     * Notifies all scenes of a window resolution change.
     *
     * @param width
     *         The new window w (in pixels).
     * @param height
     *         The new window h (in pixels).
     */
    public void resize(int width, int height) {
        for(Scene scene : scenes) {
            if(scene != null) {
                scene.resize(width, height);
            }
        }
    }

    /** Notifies all scenes to resume. */
    public void resume() {
        for(Scene scene : scenes) {
            if(scene != null) {
                scene.resume();
            }
        }
    }

    /**
     * Updates all active scenes.
     *
     * @param tt
     *         The total amount of time (in seconds) that has elapsed since the game began.
     * @param dt
     *         The amount of time (in seconds) that has elapsed since the previous frame.
     */
    public void update(double tt, double dt) {
        boolean isCovered = false;
        boolean inputHandled = false;
        for(int i = scenes.size - 1; i >= 0; i--) {
            Scene scene = scenes.get(i);
            if(scene == null) {
                scenes.removeIndex(i);
                continue;
            }
            if(scene.getState().equals(SceneState.INACTIVE) || isCovered) {
                remove(i);
            } else {
                if(scene instanceof InputProcessor && !inputHandled)
                {
                    InputProcessor inputProcessor = (InputProcessor) scene;
                    Gdx.input.setInputProcessor(inputProcessor);
                }

                scene.update(tt, dt);
                inputHandled = true;
                if(!scene.isOverlay()) {
                    isCovered = true;
                }
            }
        }
    }
}
