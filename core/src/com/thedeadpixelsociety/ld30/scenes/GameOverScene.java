package com.thedeadpixelsociety.ld30.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.thedeadpixelsociety.ld30.GameServices;
import com.thedeadpixelsociety.ld30.assets.AssetService;

public class GameOverScene extends SceneAdapter
{
    private final int damageKills;
    private final float healthRemaining;
    private final int starKills;
    private final long victims;
    private Sound buttonClickSound;
    private BitmapFont font;
    private Stage stage;
    private Viewport viewport;

    public GameOverScene(final GameServices gameServices,
                         final int starKills,
                         final int damageKills,
                         final long victims,
                         final float healthRemaining)
    {
        super(gameServices);

        this.starKills = starKills;
        this.damageKills = damageKills;
        this.victims = victims;
        this.healthRemaining = healthRemaining;
    }

    @Override public boolean isOverlay()
    {
        return true;
    }

    @Override public void dispose()
    {
        stage.dispose();
        font.dispose();
        buttonClickSound.dispose();
    }

    @Override public void draw()
    {
        stage.draw();
    }

    @Override public void resize(final int width, final int height)
    {
        viewport.setScreenSize(width, height);
        viewport.apply(true);
    }

    @Override public void show()
    {
        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport);

        buttonClickSound = Gdx.audio.newSound(Gdx.files.internal("audio/button-click.wav"));

        final Table table = new Table();
        table.setFillParent(true);

        table.row()
             .center();

        final FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/JLSDataGothicC_NC" +
                                                                                             ".otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 32;

        font = generator.generateFont(parameter);

        generator.dispose();

        final Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.WHITE);

        final Label obliterationsLabel = new Label("Obliterations:    " + damageKills + " x 500 = " + damageKills * 500,
                                                   labelStyle);
        final Label incinerationsLabel = new Label("Incinerations:    " + starKills + " x 100 = " + starKills * 100,
                                                   labelStyle);

        final long victimsPer = (victims / 1000000000l);
        final Label victimsLabel = new Label("Sentient Victims: " + victimsPer + " billion x 50 = " + victimsPer * 50l,
                                             labelStyle);
        final Label healthRemainingLabel = new Label("Health Remaining: " + (healthRemaining * 100) + "% x 10000 = " +
                                                     healthRemaining * 10000, labelStyle);
        final long score = (long) (damageKills * 500 + starKills * 100 + victimsPer * 50l + healthRemaining * 10000);
        final Label scoreLabel = new Label("Score:            " + score, labelStyle);

        final Group group = new VerticalGroup();
        group.addActor(obliterationsLabel);
        group.addActor(incinerationsLabel);
        group.addActor(victimsLabel);
        group.addActor(healthRemainingLabel);
        group.addActor(scoreLabel);

        table.add(group);

        table.row()
             .center()
             .padTop(32f);

        final AssetService assetService = gameServices.get(AssetService.class);
        final TextureAtlas ui = assetService.get("ui", TextureAtlas.class);

        final Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
        buttonStyle.up = new SpriteDrawable(ui.createSprite("button-back-unclicked"));
        buttonStyle.down = new SpriteDrawable(ui.createSprite("button-back-clicked"));

        final Button backButton = new Button(buttonStyle);
        backButton.addListener(new ClickListener(Input.Buttons.LEFT)
        {
            @Override public void clicked(final InputEvent event, final float x, final float y)
            {
                buttonClickSound.play(.3f);
                gameServices.get(SceneService.class)
                            .add(new MainMenuScene(gameServices));
            }
        });

        table.add(backButton);

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override public void update(final double tt, final double dt)
    {
        stage.act((float) dt);
    }
}
