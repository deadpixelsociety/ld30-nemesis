package com.thedeadpixelsociety.ld30;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.thedeadpixelsociety.ld30.assets.AssetService;
import com.thedeadpixelsociety.ld30.graphics.*;
import com.thedeadpixelsociety.ld30.scenes.*;

public class LD30Game extends ApplicationAdapter
{
    private static final float DT = .01f;
    private static final float MAX_DT = .16f;
    private float accumulator;
    private Camera2D gameCamera;
    private GameServices gameServices;
    private SceneService sceneService;
    private float totalDt;

    @Override
    public void create()
    {
        Units.instance.setPixelsPerUnit(16);

        gameServices = new GameServices();
        gameServices.put(new SpriteBatch());
        gameServices.put(new ShapeRenderer());
        gameServices.put(new AssetService());
        gameServices.put(gameCamera = new Camera2D());
        gameServices.put(sceneService = new SceneService());

        final AssetService assetService = gameServices.get(AssetService.class);
        assetService.loadManifest(Gdx.files.internal("json/assets.json"));
        assetService.loadGroup("game");

        assetService.finishLoading();

        SpriteBundles.instance.create(assetService.get("planets", TextureAtlas.class));
        SpriteBundles.instance.create(assetService.get("stars", TextureAtlas.class));
        SpriteBundles.instance.create(assetService.get("fragments", TextureAtlas.class));
        SpriteBundles.instance.create(assetService.get("ui", TextureAtlas.class), false);

        gameCamera.setUnitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);

        sceneService.add(new MainMenuScene(gameServices));
    }

    @Override public void resize(final int width, final int height)
    {
        gameCamera.setUnitViewport(width, height, true);
        gameCamera.update();

        sceneService.resize(width, height);
    }

    @Override
    public void render()
    {
        float dt = Gdx.graphics.getDeltaTime();

        dt = Math.min(dt, MAX_DT);

        accumulator += dt;

        while(accumulator >= DT)
        {
            update(totalDt, DT);

            totalDt += DT;
            accumulator -= DT;
        }

        draw();
    }

    @Override public void pause()
    {
        sceneService.pause();
    }

    @Override public void resume()
    {
        sceneService.resume();
    }

    @Override public void dispose()
    {
        gameServices.dispose();
    }

    private void draw()
    {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sceneService.draw();
    }

    private void update(final float totalDt, final float dt)
    {
        sceneService.update(totalDt, dt);
        gameCamera.update();
    }
}
