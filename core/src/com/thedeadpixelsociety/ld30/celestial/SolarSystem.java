package com.thedeadpixelsociety.ld30.celestial;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.thedeadpixelsociety.ld30.EntityFactory;
import com.thedeadpixelsociety.ld30.assets.AssetService;
import com.thedeadpixelsociety.ld30.components.Demographics;
import com.thedeadpixelsociety.ld30.components.Physics;
import com.thedeadpixelsociety.ld30.names.NameGenerator;
import com.thedeadpixelsociety.ld30.systems.PhysicsSystem;

public class SolarSystem
{
    private static final float BASE_ORBIT_SPEED = 16f;
    private static final float MIN_ORBIT_DISTANCE = 36f;
    private final int maxPlanets;
    private final int minPlanets;
    private final String name;
    private final NameGenerator nameGenerator;
    private final PlanetDefs planetDefs;
    private Rectangle bounds;
    private Array<Entity> planets = new Array<Entity>();
    private Entity star;

    public SolarSystem(final int minPlanets, final int maxPlanets)
    {
        this.minPlanets = minPlanets;
        this.maxPlanets = maxPlanets;

        planetDefs = PlanetDefs.load("json/planets.json");
        nameGenerator = NameGenerator.load("json/names.json");
        name = nameGenerator.getRandomName();
    }

    public void generate(final World world)
    {
        final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);

        star = EntityFactory.createStar(world);

        float orbitDistance = MIN_ORBIT_DISTANCE;
        float lastRadius = 0f;

        for(int i = 0, numPlanets = MathUtils.random(minPlanets, maxPlanets); i < numPlanets; i++)
        {
            final PlanetDef planetDef = planetDefs.getRandomDefinition();

            final PlanetSize size = planetDef.getSizes()
                                             .random();
            final float radius = size.getRadius();

            final float diameter = radius * 2f;

            orbitDistance += lastRadius + diameter + MathUtils.random() * (diameter * 1.5f);

            lastRadius = radius;

            final float angle = MathUtils.random() * 360f;

            final Vector2 position = new Vector2(1f, 0f).setAngle(angle)
                                                        .scl(orbitDistance);

            final Entity planet = EntityFactory.createPlanet(world,
                                                             planetDef.getType(),
                                                             size,
                                                             planetDef.getGravityFactor(),
                                                             nameGenerator,
                                                             position.x,
                                                             position.y);

            planets.add(planet);

            final Body starBody = star.getComponent(Physics.class)
                                      .getBody();
            final Body planetBody = planet.getComponent(Physics.class)
                                          .getBody();

            physicsSystem.createDistanceJoint(starBody, planetBody, orbitDistance);

            final int dir = MathUtils.randomBoolean() ? 1 : -1;

            final Vector2 force = planetBody.getPosition()
                                            .sub(starBody.getPosition())
                                            .nor()
                                            .rotate90(dir)
                                            .scl(BASE_ORBIT_SPEED * planetDef.getOrbitFactor());

            planetBody.setLinearVelocity(force.x, force.y);
        }

        orbitDistance += lastRadius * 2f;

        bounds = new Rectangle(-orbitDistance, -orbitDistance, orbitDistance * 2f, orbitDistance * 2f);
    }

    public Rectangle getBounds()
    {
        return bounds;
    }

    public String getName()
    {
        return name;
    }

    public Array<Entity> getPlanets()
    {
        return planets;
    }

    public Entity getStar()
    {
        return star;
    }

    public long getTotalPopulation()
    {
        long totalPopulation = 0l;

        for(Entity planet : planets)
        {
            final Demographics demographics = planet.getComponent(Demographics.class);
            if(demographics != null)
            {
                totalPopulation += demographics.getPopulation();
            }
        }

        return totalPopulation;
    }
}
