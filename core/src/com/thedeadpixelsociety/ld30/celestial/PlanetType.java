package com.thedeadpixelsociety.ld30.celestial;

public enum PlanetType
{
    ICY,
    GAS,
    MOLTEN,
    TECH,
    TERRA,
    NEMESIS
}
