package com.thedeadpixelsociety.ld30.celestial;

public enum StarColor
{
    BLUE,
    RED,
    YELLOW,
}
