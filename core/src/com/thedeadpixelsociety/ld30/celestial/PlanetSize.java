package com.thedeadpixelsociety.ld30.celestial;

import com.badlogic.gdx.math.MathUtils;

public enum PlanetSize
{
    GIANT(5, 16f, 7000000000l, 100000000000l),
    LARGE(4, 8f, 3000000000l, 15000000000l),
    MEDIUM(3, 4f, 3000000000l, 10000000000l),
    SMALL(2, 2f, 50000000l, 2000000000l);
    private long maxPopulation;
    private long minPopulation;
    private float radius;
    private int rank;

    PlanetSize(final int rank, final float radius, final long minPopulation, final long maxPopulation)
    {
        this.rank = rank;

        this.radius = radius;
        this.minPopulation = minPopulation;
        this.maxPopulation = maxPopulation;
    }

    public float getRadius()
    {
        return radius;
    }

    public long getRandomPopulation()
    {
        final float factor = MathUtils.random();
        final long diff = maxPopulation - minPopulation;
        return minPopulation + (long) (factor * diff);
    }

    public int getRank()
    {
        return rank;
    }
}
