package com.thedeadpixelsociety.ld30.celestial;

import com.badlogic.gdx.utils.*;

public class PlanetDef implements Json.Serializable
{
    private float gravityFactor;
    private float orbitFactor;
    private Array<PlanetSize> sizes = new Array<PlanetSize>();
    private PlanetType type;

    public PlanetDef()
    {
    }

    public float getGravityFactor()
    {
        return gravityFactor;
    }

    public void setGravityFactor(final float gravityFactor)
    {
        this.gravityFactor = gravityFactor;
    }

    public float getOrbitFactor()
    {
        return orbitFactor;
    }

    public void setOrbitFactor(final float orbitFactor)
    {
        this.orbitFactor = orbitFactor;
    }

    public Array<PlanetSize> getSizes()
    {
        return sizes;
    }

    public void setSizes(final Array<PlanetSize> sizes)
    {
        this.sizes = sizes;
    }

    public PlanetType getType()
    {
        return type;
    }

    public void setType(final PlanetType type)
    {
        this.type = type;
    }

    @Override public void write(final Json json)
    {
        json.writeValue("type", type);
        json.writeValue("sizes", sizes);
        json.writeValue("gravityFactor", gravityFactor);
        json.writeValue("orbitFactor", orbitFactor);
    }

    @Override public void read(final Json json, final JsonValue jsonData)
    {
        type = PlanetType.valueOf(PlanetType.class, json.readValue("type", String.class, jsonData));
        sizes = json.readValue("sizes", Array.class, PlanetSize.class, jsonData);
        gravityFactor = json.readValue("gravityFactor", Float.class, jsonData);
        orbitFactor = json.readValue("orbitFactor", Float.class, jsonData);

    }
}
