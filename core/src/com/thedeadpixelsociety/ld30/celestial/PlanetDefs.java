package com.thedeadpixelsociety.ld30.celestial;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.*;

public final class PlanetDefs implements Json.Serializable
{
    private Array<PlanetDef> definitions = new Array<PlanetDef>();

    public PlanetDefs() { }

    public static PlanetDefs load(String file)
    {
        FileHandle fileHandle = Gdx.files.internal(file);
        if(fileHandle.exists())
        {
            Json json = new Json();

            return json.fromJson(PlanetDefs.class, fileHandle);
        }

        return new PlanetDefs();
    }

    public PlanetDef getRandomDefinition()
    {
        return definitions.random();
    }

    public Array<PlanetDef> getDefinitions()
    {
        return definitions;
    }

    @Override public void write(final Json json)
    {

    }

    @Override public void read(final Json json, final JsonValue jsonData)
    {
        definitions = json.readValue("definitions", Array.class, PlanetDef.class, jsonData);
    }
}
