package com.thedeadpixelsociety.ld30;

public final class DeathSources
{
    public static final int DAMAGE = 2;
    public static final int STAR = 1;
    public static final int COLLISION = 3;
}
