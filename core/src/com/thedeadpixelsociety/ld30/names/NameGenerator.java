package com.thedeadpixelsociety.ld30.names;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

public class NameGenerator
{
    private static final int MAX_INFIXES = 3;
    private static final int MIN_INFIXES = 1;
    private static final String[] vowels = {"a", "e", "i", "o", "u", "y"};
    private Array<String> infixes = new Array<String>();
    private Array<String> prefixes = new Array<String>();
    private Array<String> suffixes = new Array<String>();

    public NameGenerator() { }

    public static NameGenerator load(String file)
    {
        FileHandle fileHandle = Gdx.files.internal(file);
        if(fileHandle.exists())
        {
            Json json = new Json();

            return json.fromJson(NameGenerator.class, fileHandle);
        }

        return new NameGenerator();
    }

    public Array<String> getInfixes()
    {
        return infixes;
    }

    public Array<String> getPrefixes()
    {
        return prefixes;
    }

    public String getRandomName()
    {
        final String prefix = getPrefixes().random();
        final Array<String> infixes = new Array<String>();

        String lastPart = prefix;
        for(int i = 0, num = MathUtils.random(MIN_INFIXES, MAX_INFIXES); i < num; i++)
        {
            String part = null;
            if(endsWithVowel(lastPart))
            {
                while(part == null)
                {
                    part = getInfixes().random();
                    if(startsWithVowel(part))
                    {
                        part = null;
                    }
                }
            }
            else
            {
                while(part == null)
                {
                    part = getInfixes().random();
                    if(!startsWithVowel(part))
                    {
                        part = null;
                    }
                }
            }

            infixes.add(part);
            lastPart = part;
        }

        String suffix = null;
        if(endsWithVowel(lastPart))
        {
            while(suffix == null)
            {
                suffix = getSuffixes().random();
                if(startsWithVowel(suffix))
                {
                    suffix = null;
                }
            }
        }
        else
        {
            while(suffix == null)
            {
                suffix = getSuffixes().random();
                if(!startsWithVowel(suffix))
                {
                    suffix = null;
                }
            }
        }

        String infix = "";
        for(String i : infixes)
        {
            infix += i;
        }

        String name = prefix + infix + suffix;
        return Character.toUpperCase(name.charAt(0)) + name.substring(1);
    }

    public Array<String> getSuffixes()
    {
        return suffixes;
    }

    private boolean endsWithVowel(String part)
    {
        if(part != null && part.length() != 0)
        {
            for(String vowel : vowels)
            {
                if(part.endsWith(vowel))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean startsWithVowel(String part)
    {
        if(part != null && part.length() != 0)
        {
            for(String vowel : vowels)
            {
                if(part.startsWith(vowel))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
