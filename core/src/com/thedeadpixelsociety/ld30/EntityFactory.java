package com.thedeadpixelsociety.ld30;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.thedeadpixelsociety.ld30.celestial.*;
import com.thedeadpixelsociety.ld30.components.*;
import com.thedeadpixelsociety.ld30.graphics.SpriteBundles;
import com.thedeadpixelsociety.ld30.names.NameGenerator;
import com.thedeadpixelsociety.ld30.systems.PhysicsSystem;

public final class EntityFactory
{
    public static Entity createFragment(final World world, final PlanetType type, final float x, final float y)
    {
        final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);

        final String spriteName = "fragment-" +
                                  type.toString()
                                      .toLowerCase();

        final Sprite sprite = SpriteBundles.instance.getSprite(spriteName);

        Entity entity = world.createEntity();

        entity.addComponent(new Tag(Tags.FRAGMENTS))
              .addComponent(new Dimensions(2f))
              .addComponent(new Health(1f))
              .addComponent(new Sprited(sprite))
              .addComponent(new Physics(physicsSystem.createCircleBody(2f, BodyDef.BodyType.DynamicBody, x, y)));

        final float angle = MathUtils.random() * 360f;

        entity.getComponent(Sprited.class)
              .getSprite()
              .setRotation(angle);

        final Body body = entity.getComponent(Physics.class)
                                .getBody();
        body.setTransform(body.getPosition(), angle);
        body.getFixtureList()
            .get(0)
            .setDensity(0f);
        body.resetMassData();
        body.setBullet(true);
        body.setLinearDamping(.7f);

        body.setUserData(entity);

        world.addEntity(entity);

        return entity;
    }

    public static Entity createPlanet(final World world,
                                      final PlanetType type,
                                      final PlanetSize size,
                                      final float gravityFactor,
                                      final NameGenerator nameGenerator,
                                      final float x,
                                      final float y)
    {
        final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);

        final String spriteName = "planet-" +
                                  size.toString()
                                      .toLowerCase() +
                                  "-" +
                                  type.toString()
                                      .toLowerCase();

        final Sprite sprite = SpriteBundles.instance.getSprite(spriteName);

        Entity entity = world.createEntity();

        entity.addComponent(new Tag(Tags.PLANETS))
              .addComponent(new Dimensions(size.getRadius()))
              .addComponent(new Health(100f))
              .addComponent(new Celestial(gravityFactor))
              .addComponent(new Sprited(sprite))
              .addComponent(new Physics(physicsSystem.createCircleBody(size.getRadius(),
                                                                       BodyDef.BodyType.DynamicBody,
                                                                       x,
                                                                       y)));

        Demographics demographics = null;
        if(MathUtils.randomBoolean(.4f))
        {
            demographics = new Demographics(nameGenerator.getRandomName(),
                                            type,
                                            size,
                                            size.getRandomPopulation(),
                                            nameGenerator.getRandomName());
        }
        else
        {
            demographics = new Demographics(nameGenerator.getRandomName(), type, size);
        }

        entity.addComponent(demographics);

        entity.getComponent(Physics.class)
              .getBody()
              .setUserData(entity);

        entity.getComponent(Physics.class)
              .getBody()
              .applyTorque(50f, true);

        world.addEntity(entity);

        return entity;
    }

    public static Entity createPlayer(final World world, final float x, final float y)
    {
        final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);

        Entity entity = world.createEntity();

        entity.addComponent(new Tag(Tags.PLAYER))
              .addComponent(new Dimensions(3f))
              .addComponent(new Demographics("Nemesis", PlanetType.NEMESIS, PlanetSize.MEDIUM))
              .addComponent(new Health(120f))
              .addComponent(new Celestial(1f))
              .addComponent(new Sprited(SpriteBundles.instance.getSprite("player-planet")))
              .addComponent(new Physics(physicsSystem.createCircleBody(3f, BodyDef.BodyType.DynamicBody, x, y)));

        entity.getComponent(Physics.class)
              .getBody()
              .setUserData(entity);

        world.addEntity(entity);

        return entity;
    }

    public static Entity createStar(final World world)
    {
        final PhysicsSystem physicsSystem = world.getSystem(PhysicsSystem.class);

        Entity entity = world.createEntity();

        final StarColor[] starColors = StarColor.values();
        final StarColor starColor = starColors[MathUtils.random(0, starColors.length - 1)];
        final String spriteName = "sun-" +
                                  starColor.toString()
                                           .toLowerCase();

        entity.addComponent(new Tag(Tags.STAR))
              .addComponent(new Dimensions(32f))
              .addComponent(new Health(999f))
              .addComponent(new Celestial(10f))
              .addComponent(new Sprited(SpriteBundles.instance.getSprite(spriteName)))
              .addComponent(new Physics(physicsSystem.createCircleBody(24f, BodyDef.BodyType.StaticBody, 0f, 0f)));

        entity.getComponent(Physics.class)
              .getBody()
              .setUserData(entity);

        world.addEntity(entity);

        return entity;
    }
}
