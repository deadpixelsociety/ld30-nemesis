/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.assets;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/** Enumerates the asset types. */
public enum AssetType {
    ATLAS(TextureAtlas.class),
    FONT(BitmapFont.class),
    MUSIC(Music.class),
    PIXMAP(Pixmap.class),
    SKIN(Skin.class),
    SOUND(Sound.class),
    TEXTURE(Texture.class);

    private final Class classType;

    /**
     * Constructs an {@link com.thedeadpixelsociety.like.assets.AssetType} object.
     *
     * @param classType
     *         The class type.
     */
    AssetType(Class classType) {
        this.classType = classType;
    }

    /**
     * Gets the asset class type.
     *
     * @return The class type.
     */
    public Class getClassType() {
        return classType;
    }
}
