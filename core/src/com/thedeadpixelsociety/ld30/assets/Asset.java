/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.assets;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/** Contains information about an asset. */
public final class Asset implements Json.Serializable
{
    private String alias;
    private String file;
    private AssetType type;

    /**
     * Gets the asset alias.
     *
     * @return The alias.
     */
    public String getAlias()
    {
        return alias;
    }

    /**
     * Gets the asset file.
     *
     * @return The file.
     */
    public String getFile()
    {
        return file;
    }

    /**
     * Gets the asset type.
     *
     * @return The type.
     */
    public AssetType getType()
    {
        return type;
    }

    @Override
    public void write(Json json)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        alias = jsonData.name();
        file = json.readValue("file", String.class, jsonData);
        type = json.readValue("type", AssetType.class, jsonData);
    }
}
