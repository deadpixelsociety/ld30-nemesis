/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.assets;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/** Contains information about a group of {@link Asset} objects. */
public final class AssetGroup implements Json.Serializable
{
    private final Array<Asset> assets = new Array<Asset>();
    private String name;

    /**
     * Gets the assets in the group.
     *
     * @return An array of {@link Asset} objects.
     */
    public Array<Asset> getAssets()
    {
        return assets;
    }

    /**
     * Gets the asset group name.
     *
     * @return The group name.
     */
    public String getName()
    {
        return name;
    }

    @Override
    public void write(Json json)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void read(Json json, JsonValue jsonData)
    {
        name = jsonData.name();
        for(JsonValue valueMap = jsonData.child(); valueMap != null; valueMap = valueMap.next())
        {
            Asset asset = new Asset();
            asset.read(json, valueMap);
            assets.add(asset);
        }
    }
}
