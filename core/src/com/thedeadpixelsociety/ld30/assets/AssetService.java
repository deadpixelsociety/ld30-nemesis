/*
 * Copyright (c) 2013. The Dead Pixel Society.  All rights reserved.
 */

package com.thedeadpixelsociety.ld30.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Decorator class for {@link com.badlogic.gdx.assets.AssetManager}.  Gives the ability to load and unload assets by
 * specified groupings.
 */
public final class AssetService implements Disposable {
    private final AssetManager assetManager = new AssetManager();
    private final ObjectMap<String, AssetGroup> assetGroupMap = new ObjectMap<String, AssetGroup>();
    private final ObjectMap<String, String> aliasFilenameMap = new ObjectMap<String, String>();

    /**
     * Gets the backing {@link com.badlogic.gdx.assets.AssetManager}.
     *
     * @return The {@link com.badlogic.gdx.assets.AssetManager}.
     */
    public AssetManager getAssetManager() {
        return assetManager;
    }

    /**
     * Gets the loading progress.
     *
     * @return The progress percentage in the range 0 to 1.
     */
    public float getProgress() {
        return assetManager.getProgress();
    }

    /** Releases all resources of this object. */
    @Override
    public void dispose() {
        assetManager.dispose();
        assetGroupMap.clear();
        aliasFilenameMap.clear();
    }

    /** Clears the asset service. */
    public void clear() {
        assetManager.clear();
        assetGroupMap.clear();
        aliasFilenameMap.clear();
    }

    /** Blocks and loads all assets. */
    public void finishLoading() {
        assetManager.finishLoading();
    }

    /**
     * Gets the specified asset.
     *
     * @param alias
     *         The asset alias.
     * @param classType
     *         The asset class type.
     * @param <T>
     *         The asset run-time class type.
     * @return The asset if it's currently loaded; otherwise, null.
     */
    public <T> T get(String alias, Class<T> classType) {
        String filename = aliasFilenameMap.get(alias);
        if(filename == null || !assetManager.isLoaded(filename)) return null;
        return assetManager.get(filename, classType);
    }

    /**
     * Gets the specified asset.
     *
     * @param filename
     *         The asset filename.
     * @param classType
     *         The asset class type.
     * @param <T>
     *         The asset run-time class type.
     * @return THe asset if it's currently loaded; otherwise, null.
     */
    public <T> T getByFile(String filename, Class<T> classType) {
        if(!assetManager.isLoaded(filename)) return null;
        return assetManager.get(filename, classType);
    }

    /**
     * Loads the specified asset group.
     *
     * @param name
     *         THe asset group name.
     */
    public void loadGroup(String name) {
        AssetGroup group = assetGroupMap.get(name);
        if(group == null) return;
        for(Asset asset : group.getAssets()) {
            aliasFilenameMap.put(asset.getAlias(), asset.getFile());
            assetManager.load(asset.getFile(), asset.getType().getClassType());
        }
    }

    /**
     * Loads the specified asset manifest.
     *
     * @param manifestFileHandle
     *         The manifest file handle.
     */
    public void loadManifest(FileHandle manifestFileHandle) {
        Json json = new Json();
        ObjectMap<String, JsonValue> assetGroups = json.fromJson(ObjectMap.class, manifestFileHandle);
        for(ObjectMap.Entry<String, JsonValue> entry : assetGroups.entries()) {
            AssetGroup assetGroup = new AssetGroup();
            assetGroup.read(json, entry.value);
            assetGroupMap.put(entry.key, assetGroup);
        }
    }

    /**
     * Unloads the specified asset group.
     *
     * @param name
     *         The asset group name.
     */
    public void unloadGroup(String name) {
        AssetGroup group = assetGroupMap.get(name);
        if(group == null) return;
        for(Asset asset : group.getAssets()) {
            assetManager.unload(asset.getFile());
            aliasFilenameMap.remove(asset.getAlias());
        }
    }

    /**
     * Updates the asset service by loading assets in its queue.
     *
     * @return true if all asset loading is finished; otherwise, false.
     */
    public boolean update() {
        return assetManager.update();
    }

    /**
     * Updates the asset service by loading assets for the specified amount of time (in milliseconds).
     *
     * @param millis
     *         The amount of time to load assets (in milliseconds).
     * @return true if all asset loading is finished; otherwise, false.
     */
    public boolean update(int millis) {
        return assetManager.update(millis);
    }
}
