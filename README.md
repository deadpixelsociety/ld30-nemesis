# Nemesis

### Overview

Inside you'll find the full Java source code and assets used for our LD30 Jam Entry, Nemesis.

### Requirements

* JDK 6+
* OpenGL 2.0+

### Compiling

The IntelliJ project is included as well as the gradle build setup. It's based off libgdx so everything should build automatically without too much issue.

### Contributors

* Coding: The Dead Pixel Society/[Web](http://www.thedeadpixelsociety.com)/[Twitter](http://www.twitter.com/deadpxlsociety)
* Art: Simple Independent Studios/[Twitter](http://www.twitter.com/thedogofdoom)